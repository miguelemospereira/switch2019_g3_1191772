/// <reference types="cypress" />
import {Login} from "../../../src/js/links/Login";

describe('Test the US005_1CreateGroupCategory', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000')
    })

    it('Test the US005.1 UI | Happy path', () => {
        //Add a page resolution
        cy.viewport(1920, 1080)

            //Log into the app
            .get(':nth-child(3) > .form-control')

            //Type the username
            .type('paulo@gmail.com', {delay: 100})

            //Click on the login button
            .get('.btn').click()

            //Click on the MyGroups NavBar link
            .get('.navbar-nav > :nth-child(2) > a').click()

            //Select the second group of the list  (in this case is Sunday Runners)
            .get(':nth-child(2) > .GroupDenomination').click()

            //Select the categories tab
            .get(':nth-child(4) > a').click()

            //Add a category named Groceries
            .get('input').type("Groceries", {delay: 100})
            .get('.Forms > .btn').click()
    })

    it('Test the US005.1 UI | The user do not have permissions to add Category to the group', () => {

        //Add a page resolution
        cy.viewport(1920, 1080)

            //Log into the app
            .get(':nth-child(3) > .form-control')

            //Type the username
            .type('paulo@gmail.com', {delay: 100})

            //Click on the login button
            .get('.btn').click()

            //Click on the MyGroups NavBar link
            .get('.navbar-nav > :nth-child(2) > a').click()

            //Select the first group of the list  (in this case is Fontes Family)
            .get(':nth-child(1) > .GroupDenomination').click()

            //Add a category named Groceries
            .get(':nth-child(4) > a').click()
            .get('input').type("Groceries", {delay: 100})
            .get('.Forms > .btn').click()
    })
})