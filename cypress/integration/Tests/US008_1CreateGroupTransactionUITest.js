/// <reference types="cypress" />
import {Login} from "../../../src/js/links/Login";

describe('Test the US008_1CreateGroupTransactionTest', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000')
    })
    it('Test the US008.1 UI | Happy path', () => {

        //Add a page resolution
        cy.viewport(1920, 1080)

            //Log into the app
            .get(':nth-child(3) > .form-control')

            //Type the username
            .type('paulo@gmail.com', {delay: 100})

            //Click on the login button
            .get('.btn').click()

            //Click ont the MyGroups NavBar link
            .get('.navbar-nav > :nth-child(2) > a').click()

            //Select the second group (in this case is Sunday Runners)
            .get(':nth-child(2) > .GroupDenomination').click()

            //Select the categories tab
            .get(':nth-child(4) > a').click()

            //Add a category named Transports
            .get('input').type("Transports", {delay: 100})
            .get('.Forms > .btn').click()

            //Select the accounts tab
            .get(':nth-child(3) > a').click()

            //Add an account with the denomination Fuel
            .get('[placeholder="Please enter denomination"]').type("Fuel", {delay: 100})
            .get('[placeholder="Please enter description"]').type("Fuel costs", {delay: 100})
            .get('.Forms > .btn').click()

            //Select the ledger tab
            .get('.homeNavBar > :nth-child(2) > a').click()

            //Add one transaction
            .get('#myCategories').select("Transports", {interval: 500})
            .get('#myType').select("debit", {interval: 100})
            .get('[type="text"]').type("July Fuel costs", {delay: 100})
            .get('[type="number"]').type("52", {delay: 100})
            .get(':nth-child(6) > #form > [type="date"]').click().type("2020-07-03", {delay: 25})
            .get('#myDebitAccount').select("Bank Account", {interval: 350})
            .get('#myCreditAccount').select("Fuel", {interval: 350})

            //Click on the add button
            .get(':nth-child(6) > #form > .btn').click()

            //Add a another transaction
            .get('#myCategories').select("Salary", {interval: 500})
            .get('#myType').select("credit", {interval: 100})
            .get('[type="text"]').type("July salary", {delay: 100})
            .get('[type="number"]').type("1050", {delay: 100})
            .get(':nth-child(6) > #form > [type="date"]').click().type("2020-07-02", {delay: 25})
            .get('#myDebitAccount').select("Company", {interval: 350})
            .get('#myCreditAccount').select("Bank Account", {interval: 350})

            //Click on the add button
            .get(':nth-child(6) > #form > .btn').click()
    })


})