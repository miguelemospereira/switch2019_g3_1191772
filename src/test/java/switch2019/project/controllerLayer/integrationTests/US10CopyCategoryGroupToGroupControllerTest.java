package switch2019.project.controllerLayer.integrationTests;

import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import switch2019.project.applicationLayer.applicationServices.US005_1CreateGroupCategoryService;
import switch2019.project.dtoLayer.dtos.CopyCategoryGroupToGroupDTO;
import switch2019.project.dtoLayer.dtos.NewGroupCategoryInfoDTO;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class US10CopyCategoryGroupToGroupControllerTest extends AbstractTest {

    @Override
    @BeforeAll
    public void setUp() {
        super.setUp();
    }


    @Test
    public void copyCategoryGroupToGroup() throws Exception {

        // Arrange

        final String personEmail = "paulo@gmail.com";
        final String groupDenominationFrom = "Fontes Family";
        final String groupDenominationTo = "Sunday Runners";
        final String categoryDenomination = "Allowance";

        final String uri = "/persons/" + personEmail + "/groups/" + groupDenominationFrom + "/categories" + "/groups/" + groupDenominationTo;

        // Input Json
        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO = new CopyCategoryGroupToGroupDTO(personEmail, groupDenominationFrom, groupDenominationTo,
                categoryDenomination);

        String inputJson = super.mapToJson(copyCategoryGroupToGroupDTO);

        // Act
        final MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        // Assert
        final int status = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY.value(), status);

        final String content = mvcResult.getResponse().getContentAsString();
        JSONObject obj = new JSONObject(content);

        // Structure
        assertTrue(obj.has("status"));
        assertTrue(obj.has("message"));
        assertTrue(obj.has("errors"));
    }

}