package switch2019.project.controllerLayer.unitTests.controllersCLI;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import switch2019.project.applicationLayer.applicationServices.US004GroupsThatAreFamilyService;
import switch2019.project.dtoLayer.dtos.GroupIDDTO;
import switch2019.project.dtoLayer.dtos.GroupsThatAreFamilyDTO;
import switch2019.project.dtoLayer.dtosAssemblers.GroupsThatAreFamilyDTOAssembler;
import switch2019.project.controllerLayer.controllers.controllersCLI.US004GroupsThatAreFamilyController;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @Fernando Silva
 */

class US004GroupsThatAreFamilyControllerTest extends AbstractTest {

    @Mock
    private US004GroupsThatAreFamilyService serviceUS004;

    @Test
    @DisplayName("Return Groups That Are Family - Success")
    public void returnGroupsThatAreFamily_Success() {

        //Arrange

        String familiaFontes = "Fontes Family";
        String familiaSilva = "Silva Family";
        String familiaPereira = "Pereira Family";

        GroupIDDTO groupIDDTOAna = new GroupIDDTO(familiaFontes);
        GroupIDDTO groupIDDTOElisabete = new GroupIDDTO(familiaSilva);
        GroupIDDTO groupIDDTOLiborio = new GroupIDDTO(familiaPereira);

        List<GroupIDDTO> groupsThatAreFamily = new ArrayList<>();
        groupsThatAreFamily.add(groupIDDTOAna);
        groupsThatAreFamily.add(groupIDDTOElisabete);
        groupsThatAreFamily.add(groupIDDTOLiborio);

        GroupsThatAreFamilyDTO groupsThatAreFamilyDTOExpected = GroupsThatAreFamilyDTOAssembler.createDTOFromDomainObject(groupsThatAreFamily);


        Mockito.when(serviceUS004.groupsThatAreFamily()).thenReturn(groupsThatAreFamilyDTOExpected);

        //Act

        US004GroupsThatAreFamilyController us004GroupsThatAreFamilyController = new US004GroupsThatAreFamilyController(serviceUS004);
        GroupsThatAreFamilyDTO result = us004GroupsThatAreFamilyController.getGroupsThatAreFamily();

        //Assert

        assertEquals(groupsThatAreFamilyDTOExpected,result);

    }

}
