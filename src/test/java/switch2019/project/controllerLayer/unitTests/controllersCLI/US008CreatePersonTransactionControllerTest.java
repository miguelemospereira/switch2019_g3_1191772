package switch2019.project.controllerLayer.unitTests.controllersCLI;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import switch2019.project.applicationLayer.applicationServices.US008CreatePersonTransactionService;
import switch2019.project.dtoLayer.dtos.CreatePersonTransactionDTO;
import switch2019.project.dtoLayer.dtos.PersonDTO;
import switch2019.project.dtoLayer.dtosAssemblers.CreatePersonTransactionDTOAssembler;
import switch2019.project.dtoLayer.dtosAssemblers.PersonDTOAssembler;
import switch2019.project.controllerLayer.controllers.controllersCLI.US008CreatePersonTransactionController;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Birthdate;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Birthplace;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Name;
import switch2019.project.domainLayer.domainEntities.vosShared.Email;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class US008CreatePersonTransactionControllerTest extends AbstractTest {

    @Mock
    private US008CreatePersonTransactionService us008Service;

    @Test
    @DisplayName("Test for createTransactionAsPerson() | Success")
    void createTransactionAsPerson_Success() {

        //ARRANGE
        //Person info
        String personEmail = "ricardo@gmail.com";
        String personName = "Ricardo";
        LocalDate birthdate = LocalDate.of(1980, 5, 20);
        String birthplace = "Porto";
        String father = "ze@gmail.com";
        String mother = "maria@gmail.com";

        //Transaction info
        String denominationCategory = "IRS";
        String type = "debit";
        String description = "January IRS";
        double amount = 150.0;
        String denominationAccountDeb = "Bank Account";
        String denominationAccountCred = "State";
        String date = LocalDate.of(2020, 5, 20).toString();

        //Arrange DTO in
        CreatePersonTransactionDTO createPersonTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(personEmail, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred, date);

        //Expected DTO out
        PersonDTO expectedPersonDTO = PersonDTOAssembler.createDTOFromDomainObject(Email.createEmail(personEmail), Name.createName(personName), Birthdate.createBirthdate(birthdate), Birthplace.createBirthplace(birthplace), PersonID.createPersonID(father), PersonID.createPersonID(mother));

        // Mock the behaviour of the service's createTransactionAsPerson method,
        // so it does not depend on other parts (e.g. DB)
        Mockito.when(us008Service.createTransactionAsPerson(createPersonTransactionDTO)).thenReturn(expectedPersonDTO);

        //Controller
        US008CreatePersonTransactionController us008Controller = new US008CreatePersonTransactionController(us008Service);


        //ACT
        PersonDTO result = us008Controller.createTransactionAsPerson(personEmail, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred, date);


        //ASSERT
        assertEquals(expectedPersonDTO, result);
    }

    @Test
    @DisplayName("Test for createTransactionAsPerson() | Fail | Person does not exist")
    void createTransactionAsPerson_Fail_PersonDoesNotExist() {

        //ARRANGE
        //Person info
        String personEmail = "ricardo@gmail.com";

        //Transaction info
        String denominationCategory = "IRS";
        String type = "debit";
        String description = "January IRS";
        double amount = 150.0;
        String denominationAccountDeb = "Bank Account";
        String denominationAccountCred = "State";
        String date = LocalDate.of(2020, 5, 20).toString();

        //Arrange DTO in
        CreatePersonTransactionDTO createPersonTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(personEmail, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred, date);

        //Mock the behaviour of the service's createTransactionAsPerson method,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us008Service.createTransactionAsPerson(createPersonTransactionDTO)).thenThrow(new NotFoundArgumentsBusinessException(US008CreatePersonTransactionService.PERSON_DOES_NOT_EXIST));

        //Expected message
        String expectedMessage = "Person doesn't exist";

        //Controller
        US008CreatePersonTransactionController us008Controller = new US008CreatePersonTransactionController(us008Service);


        //ACT
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us008Controller.createTransactionAsPerson(personEmail, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred, date));


        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    @DisplayName("Test for createTransactionAsPerson() | Fail | Category does not exist")
    void createTransactionAsPerson_Fail_CategoryDoesNotExist() {

        //ARRANGE
        //Person info
        String personEmail = "ricardo@gmail.com";

        //Transaction info
        String denominationCategory = "IRS";
        String type = "debit";
        String description = "January IRS";
        double amount = 150.0;
        String denominationAccountDeb = "Bank Account";
        String denominationAccountCred = "State";
        String date = LocalDate.of(2020, 5, 20).toString();

        //Arrange DTO in
        CreatePersonTransactionDTO createPersonTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(personEmail, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred, date);

        //Mock the behaviour of the service's createTransactionAsPerson method,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us008Service.createTransactionAsPerson(createPersonTransactionDTO)).thenThrow(new NotFoundArgumentsBusinessException(US008CreatePersonTransactionService.CATEGORY_DOES_NOT_EXIST));

        //Expected message
        String expectedMessage = "Category doesn't exist";

        //Controller
        US008CreatePersonTransactionController us008Controller = new US008CreatePersonTransactionController(us008Service);


        //ACT
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us008Controller.createTransactionAsPerson(personEmail, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred, date));


        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    @DisplayName("Test for createTransactionAsPerson() | Fail | Debit Account does not exist")
    void createTransactionAsPerson_Fail_DebitAccountDoesNotExist() {

        //ARRANGE
        //Person info
        String personEmail = "ricardo@gmail.com";

        //Transaction info
        String denominationCategory = "IRS";
        String type = "debit";
        String description = "January IRS";
        double amount = 150.0;
        String denominationAccountDeb = "Bank Account";
        String denominationAccountCred = "State";
        String date = LocalDate.of(2020, 5, 20).toString();

        //Arrange DTO in
        CreatePersonTransactionDTO createPersonTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(personEmail, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred, date);

        //Mock the behaviour of the service's createTransactionAsPerson method,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us008Service.createTransactionAsPerson(createPersonTransactionDTO)).thenThrow(new NotFoundArgumentsBusinessException(US008CreatePersonTransactionService.ACCOUNT_DEB_DOES_NOT_EXIST));

        //Expected message
        String expectedMessage = "Debit Account doesn't exist";

        //Controller
        US008CreatePersonTransactionController us008Controller = new US008CreatePersonTransactionController(us008Service);


        //ACT
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us008Controller.createTransactionAsPerson(personEmail, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred, date));


        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    @DisplayName("Test for createTransactionAsPerson() | Fail | Credit Account does not exist")
    void createTransactionAsPerson_Fail_CreditAccountDoesNotExist() {

        //ARRANGE
        //Person info
        String personEmail = "ricardo@gmail.com";

        //Transaction info
        String denominationCategory = "IRS";
        String type = "debit";
        String description = "January IRS";
        double amount = 150.0;
        String denominationAccountDeb = "Bank Account";
        String denominationAccountCred = "State";
        String date = LocalDate.of(2020, 5, 20).toString();

        //Arrange DTO in
        CreatePersonTransactionDTO createPersonTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(personEmail, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred, date);

        //Mock the behaviour of the service's createTransactionAsPerson method,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us008Service.createTransactionAsPerson(createPersonTransactionDTO)).thenThrow(new NotFoundArgumentsBusinessException(US008CreatePersonTransactionService.ACCOUNT_CRED_DOES_NOT_EXIST));

        //Expected message
        String expectedMessage = "Credit Account doesn't exist";

        //Controller
        US008CreatePersonTransactionController us008Controller = new US008CreatePersonTransactionController(us008Service);


        //ACT
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us008Controller.createTransactionAsPerson(personEmail, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred, date));


        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }
}