package switch2019.project.dtoLayer.dtosAssemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import switch2019.project.dtoLayer.dtos.CopyCategoryGroupToGroupDTO;
import switch2019.project.dtoLayer.dtos.CreateGroupDTO;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CopyCategoryGroupToGroupDTOAssemblerTest {

    @Test
    @DisplayName("test create data transfer assembler")
    void testCreateDataTransferObjectWithPrimitives(){

        // Arrange
        String email = "miguel@gmail.com";
        String denominationFrom = "Group A";   // group denomination
        String denominationTo = "Group B";     // group denomination
        String categoryDenomination = "Sports";

        // Expected
        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTOexpected = new CopyCategoryGroupToGroupDTO(
                email, denominationFrom, denominationTo, categoryDenomination);


        // Act
        CopyCategoryGroupToGroupDTOAssembler copyCategoryGroupToGroupDTOAssembler = new CopyCategoryGroupToGroupDTOAssembler();

        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO = copyCategoryGroupToGroupDTOAssembler.createDTOFromPrimitiveTypes(
                email, denominationFrom, denominationTo, categoryDenomination);


        // Assert
        assertEquals(copyCategoryGroupToGroupDTOexpected, copyCategoryGroupToGroupDTO);
        assertEquals(email, copyCategoryGroupToGroupDTO.getPersonEmail());
        assertEquals(denominationFrom, copyCategoryGroupToGroupDTO.getGroupDenominationFrom());
        assertEquals(denominationTo, copyCategoryGroupToGroupDTO.getGroupDenominationTo());
        assertEquals(categoryDenomination, copyCategoryGroupToGroupDTO.getCategoryDenomination());
    }

}