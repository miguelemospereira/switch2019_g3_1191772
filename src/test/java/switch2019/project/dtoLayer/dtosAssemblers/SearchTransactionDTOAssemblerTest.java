package switch2019.project.dtoLayer.dtosAssemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import switch2019.project.dtoLayer.dtos.SearchTransactionDTO;

import static org.junit.jupiter.api.Assertions.*;
/**
 * @author Ala Matos
 */
class SearchTransactionDTOAssemblerTest {


    @Test
    @DisplayName("Test DTO constructor")
    void testConstructor () {
        //Arrange
        String personID= "paulo@gmail.com";
        String startDate = "2020-02-12";
        String endDate = "2020-03-12";
        //Act
        SearchTransactionDTO actualSearchTransactionDTOAssembler= SearchTransactionDTOAssembler.createDTOFromPrimitiveType(personID, startDate, endDate);
        SearchTransactionDTO expectedSearchTransactionDTOAssembler= new SearchTransactionDTO(personID, startDate, endDate);
        //Assert
        assertEquals(expectedSearchTransactionDTOAssembler, actualSearchTransactionDTOAssembler);
    }
}