package switch2019.project.dtoLayer.dtos;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CopyCategoryGroupToGroupDTOTest {

    // 1. Test constructor

    @Test
    @DisplayName("test constructor")
    void CategoryGroupToGroup_Constructor() {

        // Arrange
        String email = "miguel@gmail.com";
        String denominationFrom = "Group A";   // group denomination
        String denominationTo = "Group B";     // group denomination
        String categoryDenomination = "Sports";

        // Act
        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO = new CopyCategoryGroupToGroupDTO(
                email, denominationFrom, denominationTo, categoryDenomination);

        // Assert
        assertEquals(email, copyCategoryGroupToGroupDTO.getPersonEmail());
        assertEquals(denominationFrom, copyCategoryGroupToGroupDTO.getGroupDenominationFrom());
        assertEquals(denominationTo, copyCategoryGroupToGroupDTO.getGroupDenominationTo());
        assertEquals(categoryDenomination, copyCategoryGroupToGroupDTO.getCategoryDenomination());
    }


    // 2. Test equals

    @Test
    @DisplayName("test equals - same object")
    void Equals_SameObject() {

        // Arrange
        String email = "miguel@gmail.com";
        String denominationFrom = "Group A";   // group denomination
        String denominationTo = "Group B";     // group denomination
        String categoryDenomination = "Sports";

        // Act
        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO = new CopyCategoryGroupToGroupDTO(
                email, denominationFrom, denominationTo, categoryDenomination);

        boolean result = copyCategoryGroupToGroupDTO.equals(copyCategoryGroupToGroupDTO);

        // Assert
        assertEquals(true, result);
        assertTrue(result);
    }


    @Test
    @DisplayName("test equals - same information")
    void Equals_DifferentObjectsSameInformation() {

        // Arrange
        String email = "miguel@gmail.com";
        String denominationFrom = "Group A";   // group denomination
        String denominationTo = "Group B";     // group denomination
        String categoryDenomination = "Sports";

        // Act
        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO1 = new CopyCategoryGroupToGroupDTO(
                email, denominationFrom, denominationTo, categoryDenomination);

        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO2 = new CopyCategoryGroupToGroupDTO(
                email, denominationFrom, denominationTo, categoryDenomination);

        boolean result = copyCategoryGroupToGroupDTO1.equals(copyCategoryGroupToGroupDTO2);

        // Assert
        assertEquals(copyCategoryGroupToGroupDTO1.equals(copyCategoryGroupToGroupDTO2), copyCategoryGroupToGroupDTO2.equals(copyCategoryGroupToGroupDTO1));
        assertEquals(true, result);
        assertTrue(result);
    }


    @Test
    @DisplayName("test equals - same data")
    void Equals_DifferentObjectsSameData() {

        // Arrange 1
        String email1 = "miguel@gmail.com";
        String denominationFrom1 = "Group A";   // group denomination
        String denominationTo1 = "Group B";     // group denomination
        String categoryDenomination1 = "Sports";

        // Arrange 2
        String email2 = "miguel@gmail.com";
        String denominationFrom2 = "Group A";   // group denomination
        String denominationTo2 = "Group B";     // group denomination
        String categoryDenomination2 = "Sports";

        // Act
        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO1 = new CopyCategoryGroupToGroupDTO(
                email1, denominationFrom1, denominationTo1, categoryDenomination1);

        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO2 = new CopyCategoryGroupToGroupDTO(
                email2, denominationFrom2, denominationTo2, categoryDenomination2);

        boolean result = copyCategoryGroupToGroupDTO1.equals(copyCategoryGroupToGroupDTO2);

        // Assert
        assertEquals(copyCategoryGroupToGroupDTO1.equals(copyCategoryGroupToGroupDTO2), copyCategoryGroupToGroupDTO2.equals(copyCategoryGroupToGroupDTO1));
        assertEquals(true, result);
        assertTrue(result);
    }


    @Test
    @DisplayName("test equals - different information - Email")
    void Equals_DifferentObjectsDifferentInformation_Email() {

        // Arrange1
        String email1 = "miguel@gmail.com";
        String denominationFrom1 = "Group A";   // group denomination
        String denominationTo1 = "Group B";     // group denomination
        String categoryDenomination1 = "Sports";

        // ArrangeB
        String email2 = "lemos@gmail.com";
        String denominationFrom2 = "Group A";   // group denomination
        String denominationTo2 = "Group B";     // group denomination
        String categoryDenomination2 = "Sports";

        // Act
        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO1 = new CopyCategoryGroupToGroupDTO(
                email1, denominationFrom1, denominationTo1, categoryDenomination1);

        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO2 = new CopyCategoryGroupToGroupDTO(
                email2, denominationFrom2, denominationTo2, categoryDenomination2);

        boolean result = copyCategoryGroupToGroupDTO1.equals(copyCategoryGroupToGroupDTO2);

        // Assert
        assertEquals(false, result);
        assertFalse(result);
    }


    @Test
    @DisplayName("test equals - different information - denominationFrom")
    void Equals_DifferentObjectsDifferentInformation_denominationFrom() {

        // Arrange1
        String email1 = "miguel@gmail.com";
        String denominationFrom1 = "Skaters Group";   // group denomination
        String denominationTo1 = "Group B";     // group denomination
        String categoryDenomination1 = "Sports";

        // ArrangeB
        String email2 = "miguel@gmail.com";
        String denominationFrom2 = "Group A";   // group denomination
        String denominationTo2 = "Group B";     // group denomination
        String categoryDenomination2 = "Sports";

        // Act
        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO1 = new CopyCategoryGroupToGroupDTO(
                email1, denominationFrom1, denominationTo1, categoryDenomination1);

        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO2 = new CopyCategoryGroupToGroupDTO(
                email2, denominationFrom2, denominationTo2, categoryDenomination2);

        boolean result = copyCategoryGroupToGroupDTO1.equals(copyCategoryGroupToGroupDTO2);

        // Assert
        assertEquals(false, result);
        assertFalse(result);
    }


    @Test
    @DisplayName("test equals - different information - denominationTo")
    void Equals_DifferentObjectsDifferentInformation_denominationTo() {

        // Arrange1
        String email1 = "miguel@gmail.com";
        String denominationFrom1 = "Group A";   // group denomination
        String denominationTo1 = "Skaters Group";     // group denomination
        String categoryDenomination1 = "Sports";

        // ArrangeB
        String email2 = "miguel@gmail.com";
        String denominationFrom2 = "Group A";   // group denomination
        String denominationTo2 = "Group B";     // group denomination
        String categoryDenomination2 = "Sports";

        // Act
        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO1 = new CopyCategoryGroupToGroupDTO(
                email1, denominationFrom1, denominationTo1, categoryDenomination1);

        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO2 = new CopyCategoryGroupToGroupDTO(
                email2, denominationFrom2, denominationTo2, categoryDenomination2);

        boolean result = copyCategoryGroupToGroupDTO1.equals(copyCategoryGroupToGroupDTO2);

        // Assert
        assertEquals(false, result);
        assertFalse(result);
    }


    @Test
    @DisplayName("test equals - NotIstanceOf")
    void Equals_NotInstanceOf() {

        // Arrange
        String email = "miguel@gmail.com";
        String denominationFrom = "Group A";   // group denomination
        String denominationTo = "Group B";     // group denomination
        String categoryDenomination = "Sports";

        // Act
        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO = new CopyCategoryGroupToGroupDTO(
                email, denominationFrom, denominationTo, categoryDenomination);


        boolean result = copyCategoryGroupToGroupDTO.equals(email);

        // Assert

        assertEquals(false, result);
        assertFalse(result);
    }


    @Test
    @DisplayName("test equals - Null")
    void Equals_Null() {

        // Arrange1
        String email1 = "miguel@gmail.com";
        String denominationFrom1 = "Group A";   // group denomination
        String denominationTo1 = "Group B";     // group denomination
        String categoryDenomination1 = "Sports";

        // ArrangeB
        String email2 = "miguel@gmail.com";
        String denominationFrom2 = "Group A";   // group denomination
        String denominationTo2 = "Group B";     // group denomination
        String categoryDenomination2 = "Sports";

        // Act
        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO1 = new CopyCategoryGroupToGroupDTO(
                email1, denominationFrom1, denominationTo1, categoryDenomination1);

        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO2 = null;

        boolean result = copyCategoryGroupToGroupDTO1.equals(copyCategoryGroupToGroupDTO2);

        // Assert
        assertEquals(false, result);
        assertFalse(result);
    }


    // 3. Test hashCode

    @Test
    @DisplayName("test hashCode - True")
    void hashCode_True() {

        // Arrange
        String email = "miguel@gmail.com";
        String denominationFrom = "Group A";   // group denomination
        String denominationTo = "Group B";     // group denomination
        String categoryDenomination = "Sports";

        // Act
        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO1 = new CopyCategoryGroupToGroupDTO(
                email, denominationFrom, denominationTo, categoryDenomination);

        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO2 = new CopyCategoryGroupToGroupDTO(
                email, denominationFrom, denominationTo, categoryDenomination);

        boolean result = copyCategoryGroupToGroupDTO1.hashCode() == copyCategoryGroupToGroupDTO2.hashCode();

        // Assert
        assertTrue(copyCategoryGroupToGroupDTO1.hashCode() == copyCategoryGroupToGroupDTO2.hashCode());
        assertEquals(true, result);
        assertTrue(result);
    }


    @Test
    @DisplayName("test hashCode - False")
    void hashCode_false() {

        // Arrange A
        String email1 = "miguel@gmail.com";
        String denominationFrom1 = "Group A";   // group denomination
        String denominationTo1 = "Group B";     // group denomination
        String categoryDenomination1 = "Sports";

        // Arrange B
        String email2 = "lemos@gmail.com";
        String denominationFrom2 = "Group A";   // group denomination
        String denominationTo2 = "Group B";     // group denomination
        String categoryDenomination2 = "Sports";

        // Act
        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO1 = new CopyCategoryGroupToGroupDTO(
                email1, denominationFrom1, denominationTo1, categoryDenomination1);

        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO2 = new CopyCategoryGroupToGroupDTO(
                email2, denominationFrom2, denominationTo2, categoryDenomination2);

        boolean result = copyCategoryGroupToGroupDTO1.hashCode() == copyCategoryGroupToGroupDTO2.hashCode();

        // Assert
        assertFalse(copyCategoryGroupToGroupDTO1.hashCode() == copyCategoryGroupToGroupDTO2.hashCode());
        assertEquals(false, result);
        assertFalse(result);
    }


}