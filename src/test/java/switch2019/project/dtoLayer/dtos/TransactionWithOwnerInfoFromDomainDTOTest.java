package switch2019.project.dtoLayer.dtos;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


/**
 * @author Ala Matos
 */
class TransactionWithOwnerInfoFromDomainDTOTest {

    private String ownerInfo;
    private String category;
    private String type;
    private String description;
    private double amount;
    private String date;
    private String debitAccount;
    private String creditAccount;

    @BeforeEach
    public void init() {

//        Arrange the needed info to create a TransactionWithOwnerInfoFromDomainDTO
        ownerInfo = "Hulk";
        category = "grocery";
        type = "debit";
        description = "month groceries";
        amount = 50;
        date = "2020-06-22";
        debitAccount = "wallet";
        creditAccount = "SuperMarket";

    }

    @Test
    @DisplayName("Test DTO constructor with parameters")
    void testConstructor() {

//        Act -> Instantiate a new TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO actualTransactionWithOwnerInfoFromDomainDTO = new TransactionWithOwnerInfoFromDomainDTO(ownerInfo,
                category, type, description, amount, date, debitAccount, creditAccount);

//        Assert
        assertEquals(ownerInfo, actualTransactionWithOwnerInfoFromDomainDTO.getOwnerInfo());
        assertEquals(category, actualTransactionWithOwnerInfoFromDomainDTO.getCategory());
        assertEquals(type, actualTransactionWithOwnerInfoFromDomainDTO.getType());
        assertEquals(description, actualTransactionWithOwnerInfoFromDomainDTO.getDescription());
        assertEquals(amount, actualTransactionWithOwnerInfoFromDomainDTO.getAmount());
        assertEquals(date, actualTransactionWithOwnerInfoFromDomainDTO.getDate());
        assertEquals(debitAccount, actualTransactionWithOwnerInfoFromDomainDTO.getDebitAccount());
        assertEquals(creditAccount, actualTransactionWithOwnerInfoFromDomainDTO.getCreditAccount());
    }

    @Test
    @DisplayName("Test set methods")
    void testSetMethods() {

//        Act -> Set all the parameters of the TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO actualTransactionWithOwnerInfoFromDomainDTO = new TransactionWithOwnerInfoFromDomainDTO();
        actualTransactionWithOwnerInfoFromDomainDTO.setOwnerInfo(ownerInfo);
        actualTransactionWithOwnerInfoFromDomainDTO.setCategory(category);
        actualTransactionWithOwnerInfoFromDomainDTO.setType(type);
        actualTransactionWithOwnerInfoFromDomainDTO.setDescription(description);
        actualTransactionWithOwnerInfoFromDomainDTO.setAmount(amount);
        actualTransactionWithOwnerInfoFromDomainDTO.setDate(date);
        actualTransactionWithOwnerInfoFromDomainDTO.setDebitAccount(debitAccount);
        actualTransactionWithOwnerInfoFromDomainDTO.setCreditAccount(creditAccount);

//        Assert
        assertEquals(ownerInfo, actualTransactionWithOwnerInfoFromDomainDTO.getOwnerInfo());
        assertEquals(category, actualTransactionWithOwnerInfoFromDomainDTO.getCategory());
        assertEquals(type, actualTransactionWithOwnerInfoFromDomainDTO.getType());
        assertEquals(description, actualTransactionWithOwnerInfoFromDomainDTO.getDescription());
        assertEquals(amount, actualTransactionWithOwnerInfoFromDomainDTO.getAmount());
        assertEquals(date, actualTransactionWithOwnerInfoFromDomainDTO.getDate());
        assertEquals(debitAccount, actualTransactionWithOwnerInfoFromDomainDTO.getDebitAccount());
        assertEquals(creditAccount, actualTransactionWithOwnerInfoFromDomainDTO.getCreditAccount());
    }

    @Test
    @DisplayName("Test equals method | Two equals TransactionWithOwnerInfoFromDomainDTO")
    void testEqualsMethod() {

//        Act -> Instantiate a TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO firstTransactionWithOwnerInfoFromDomainDTO = new TransactionWithOwnerInfoFromDomainDTO(ownerInfo,
                category, type, description, amount, date, debitAccount, creditAccount);

//        Act -> Instantiate another TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO secondTransactionWithOwnerInfoFromDomainDTO = new TransactionWithOwnerInfoFromDomainDTO(ownerInfo,
                 category, type, description, amount, date, debitAccount, creditAccount);

//        Assert
        assertEquals(firstTransactionWithOwnerInfoFromDomainDTO, secondTransactionWithOwnerInfoFromDomainDTO);
    }

    @Test
    @DisplayName("Test equals method | Two different TransactionWithOwnerInfoFromDomainDTO")
    void testEqualsMethodDifferentTransactions() {

//        Arrange the needed info to create a TransactionWithOwnerInfoFromDomainDTO
        String wolverineTransactionInfo = "Wolverine";
        String wolverineTransactionCategory = "Salary";
        String wolverineTransactionType = "credit";
        String wolverineTransactionDescription = "Wage";
        double wolverineTransactionAmount = 1050;
        String wolverineTransactionDate = "2020-06-28";
        String wolverineTransactionDebitAccount = "Company";
        String wolverineTransactionCreditAccount = "wallet";

//        Act
        TransactionWithOwnerInfoFromDomainDTO firstTransactionWithOwnerInfoFromDomainDTO = new TransactionWithOwnerInfoFromDomainDTO(ownerInfo,
                category, type, description, amount, date, debitAccount, creditAccount);

//        Act -> Instantiate a TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO secondTransactionWithOwnerInfoFromDomainDTO = new TransactionWithOwnerInfoFromDomainDTO(wolverineTransactionInfo,
                wolverineTransactionCategory, wolverineTransactionType, wolverineTransactionDescription, wolverineTransactionAmount, wolverineTransactionDate,
                wolverineTransactionDebitAccount, wolverineTransactionCreditAccount);

//        Assert
        assertNotEquals(firstTransactionWithOwnerInfoFromDomainDTO, secondTransactionWithOwnerInfoFromDomainDTO);
    }

    @Test
    @DisplayName("Test equals method | Two equals TransactionWithOwnerInfoFromDomainDTO")
    void testHashCodeMethodSameTransaction() {

//        Act -> Instantiate a TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO firstTransactionWithOwnerInfoFromDomainDTO = new TransactionWithOwnerInfoFromDomainDTO(ownerInfo,
                category, type, description, amount, date, debitAccount, creditAccount);

//        Act -> Instantiate another TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO secondTransactionWithOwnerInfoFromDomainDTO = new TransactionWithOwnerInfoFromDomainDTO(ownerInfo,
                category, type, description, amount, date, debitAccount, creditAccount);

//        Assert
        assertEquals(firstTransactionWithOwnerInfoFromDomainDTO.hashCode(), secondTransactionWithOwnerInfoFromDomainDTO.hashCode());
    }

    @Test
    @DisplayName("Test Hashcode method | Two different TransactionWithOwnerInfoFromDomainDTO")
    void testHashCodeMethodDifferentTransactions() {

//        Arrange the needed info to create a TransactionWithOwnerInfoFromDomainDTO
        String wolverineTransactionInfo = "Wolverine";
        String wolverineTransactionCategory = "Salary";
        String wolverineTransactionType = "credit";
        String wolverineTransactionDescription = "Wage";
        double wolverineTransactionAmount = 1050;
        String wolverineTransactionDate = "2020-06-28";
        String wolverineTransactionDebitAccount = "Company";
        String wolverineTransactionCreditAccount = "wallet";

//        Act -> Instantiate a TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO firstTransactionWithOwnerInfoFromDomainDTO = new TransactionWithOwnerInfoFromDomainDTO(ownerInfo,
                category, type, description, amount, date, debitAccount, creditAccount);

//        Act -> Instantiate another TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO secondTransactionWithOwnerInfoFromDomainDTO = new TransactionWithOwnerInfoFromDomainDTO(wolverineTransactionInfo,
                wolverineTransactionCategory, wolverineTransactionType, wolverineTransactionDescription, wolverineTransactionAmount, wolverineTransactionDate,
                wolverineTransactionDebitAccount, wolverineTransactionCreditAccount);

//        Assert
        assertNotEquals(firstTransactionWithOwnerInfoFromDomainDTO.hashCode(), secondTransactionWithOwnerInfoFromDomainDTO.hashCode());
    }

}