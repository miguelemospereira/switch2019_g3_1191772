package switch2019.project.dtoLayer.dtos;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * @author Ala Matos
 */
class SearchTransactionDTOTest {

    private String pauloName;
    private String pauloStartDate;
    private String pauloEndDate;


    @BeforeEach
    void init() {

//      Arrange SearchTransactionDTO
       pauloName = "paulo@gmail.com";
       pauloStartDate = "2020-02-12";
       pauloEndDate = "2020-03-12";
    }

    @Test
    @DisplayName("Test the SearchTransactionDTO constructor")
    void testDTOConstructor() {

//        Act
        SearchTransactionDTO actualSearchTransactionDTO = new SearchTransactionDTO(pauloName, pauloStartDate,pauloEndDate);

//        Assert
        assertEquals(pauloName, actualSearchTransactionDTO.getPersonID());
        assertEquals(pauloStartDate, actualSearchTransactionDTO.getStartDate());
        assertEquals(pauloEndDate, actualSearchTransactionDTO.getEndDate());
    }

    @Test
    @DisplayName("Test the equals method | Two equals SearchTransactionDTO")
    void testEqualsMethod() {

//        Act
        SearchTransactionDTO firstSearchTransactionDTO = new SearchTransactionDTO(pauloName, pauloStartDate,pauloEndDate);
        SearchTransactionDTO secondSearchTransactionDTO = new SearchTransactionDTO(pauloName, pauloStartDate,pauloEndDate);

//        Assert
        assertEquals(firstSearchTransactionDTO, secondSearchTransactionDTO);
    }

    @Test
    @DisplayName("Test the equals method | Two different SearchTransactionDTO")
    void testEqualsMethodDifferentSearchTransactionDTO() {

//        Arrange second SearchTransactionDTO
        String henriqueName = "henrique@gmail.com";
        String henriqueStartDate = "2020-01-12";
        String henriqueEndDate = "2020-04-12";

//        Act
        SearchTransactionDTO firstSearchTransactionDTO = new SearchTransactionDTO(pauloName, pauloStartDate,pauloEndDate);
        SearchTransactionDTO secondSearchTransactionDTO = new SearchTransactionDTO(henriqueName, henriqueStartDate, henriqueEndDate);

//        Assert
        assertNotEquals(firstSearchTransactionDTO, secondSearchTransactionDTO);
    }

    @Test
    @DisplayName("Test the hashCode method | Two equals SearchTransactionDTO")
    void testHashCodeMethod() {

//        Act
        SearchTransactionDTO firstSearchTransactionDTO = new SearchTransactionDTO(pauloName, pauloStartDate,pauloEndDate);
        SearchTransactionDTO secondSearchTransactionDTO = new SearchTransactionDTO(pauloName, pauloStartDate,pauloEndDate);

//        Assert
        assertEquals(firstSearchTransactionDTO.hashCode(), secondSearchTransactionDTO.hashCode());
    }

    @Test
    @DisplayName("Test the equals method | Two different SearchTransactionDTO")
    void testHashCodeMethodDifferentSearchTransactionDTO() {

//        Arrange second SearchTransactionDTO
        String henriqueName = "henrique@gmail.com";
        String henriqueStartDate = "2020-01-12";
        String henriqueEndDate = "2020-04-12";

//        Act
        SearchTransactionDTO firstSearchTransactionDTO = new SearchTransactionDTO(pauloName, pauloStartDate,pauloEndDate);
        SearchTransactionDTO secondSearchTransactionDTO = new SearchTransactionDTO(henriqueName, henriqueStartDate, henriqueEndDate);

//        Assert
        assertNotEquals(firstSearchTransactionDTO.hashCode(), secondSearchTransactionDTO.hashCode());
    }

}