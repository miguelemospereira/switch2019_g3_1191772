package switch2019.project.applicationLayer.applicationServices;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.aggregates.group.Group;
import switch2019.project.domainLayer.domainEntities.vosShared.*;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;
import switch2019.project.domainLayer.repositoriesInterfaces.ICategoryRepository;
import switch2019.project.domainLayer.repositoriesInterfaces.IGroupRepository;
import switch2019.project.dtoLayer.dtos.CopyCategoryGroupToGroupDTO;
import switch2019.project.dtoLayer.dtos.CreateGroupCategoryDTO;
import switch2019.project.dtoLayer.dtos.GroupDTO;
import switch2019.project.dtoLayer.dtosAssemblers.CopyCategoryGroupToGroupDTOAssembler;
import switch2019.project.dtoLayer.dtosAssemblers.CreateGroupCategoryDTOAssembler;
import switch2019.project.dtoLayer.dtosAssemblers.GroupDTOAssembler;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class US10CopyCategoryGroupToGroupServiceTest extends AbstractTest {

    @Mock
    private IGroupRepository groupRepository;
    @Mock
    private ICategoryRepository categoryRepository;

    private US10CopyCategoryGroupToGroupService service;

    // Group A - Group From
    private Group fontesFamily;
    private GroupID fontesFamilyID;

    // Group B - Group To (admin - paulo)
    private Group lemosFamily;
    private GroupID lemosFamilyID;

    @BeforeEach
    public void init() {

        // Manuel
        String manuelEmail = "manuel@gmail.com";
        PersonID manuelPersonID = PersonID.createPersonID(manuelEmail);

        // Ilda
        String ildaEmail = "ilda@gmail.com";
        PersonID ildaPersonID = PersonID.createPersonID(ildaEmail);

        // Paulo
        String pauloEmail = "paulo@gmail.com";
        PersonID pauloPersonID = PersonID.createPersonID(pauloEmail);

        // Helder
        String helderEmail = "helder@gmail.com";
        PersonID helderPersonID = PersonID.createPersonID(helderEmail);


        // Family Fontes [Group A]
        // Categories ->          Salary / Draw Money / IRS / Food / Water Bill / Netflix

        String dateOfCreation = "2020-06-01";
        String fontesFamilyDenomination = "Fontes Family";
        String fontesFamilyDescription = "All members from Fontes family";

        this.fontesFamily = Group.createGroup(fontesFamilyDenomination, fontesFamilyDescription, dateOfCreation, manuelPersonID);
        this.fontesFamilyID = GroupID.createGroupID(fontesFamilyDenomination);
        fontesFamily.addPersonInCharge(ildaPersonID);
        fontesFamily.addMember(pauloPersonID);
        fontesFamily.addMember(helderPersonID);

        // Salary

        String salaryDenomination = "Salary";
        CategoryID salaryID = CategoryID.createCategoryID(salaryDenomination, fontesFamilyID);
        fontesFamily.addCategory(salaryID);

        // Draw Money

        String drawMoneyDenomination = "Draw Money";
        CategoryID drawMoneyID = CategoryID.createCategoryID(drawMoneyDenomination, fontesFamilyID);
        fontesFamily.addCategory(drawMoneyID);

        // IRS

        String irsDenomination = "IRS";
        CategoryID irsID = CategoryID.createCategoryID(irsDenomination, fontesFamilyID);
        fontesFamily.addCategory(irsID);

        // Food

        String foodDenomination = "Food";
        CategoryID foodID = CategoryID.createCategoryID(foodDenomination, fontesFamilyID);
        fontesFamily.addCategory(foodID);


        // Water Bill

        String waterBillDenomination = "Water Bill";
        CategoryID waterBillID = CategoryID.createCategoryID(waterBillDenomination, fontesFamilyID);
        fontesFamily.addCategory(waterBillID);


        // Netflix

        String netflixDenomination = "Netflix";
        CategoryID netflixID = CategoryID.createCategoryID(netflixDenomination, fontesFamilyID);
        fontesFamily.addCategory(netflixID);


        // Family Lemos [Group B]
        // Categories ->         Draw Money / IRS / Food / Water Bill / Netflix   -->   (no Salary)

        String dateOfCreation2 = "2020-06-01";
        String lemosFamilyDenomination = "Lemos Family";
        String lemosFamilyDescription = "All members from Lemos family";

        this.lemosFamily = Group.createGroup(lemosFamilyDenomination, lemosFamilyDescription, dateOfCreation2, pauloPersonID);
        this.fontesFamilyID = GroupID.createGroupID(fontesFamilyDenomination);
        this.lemosFamilyID = GroupID.createGroupID(lemosFamilyDenomination);

        fontesFamily.addPersonInCharge(pauloPersonID);
        fontesFamily.addMember(pauloPersonID);
        fontesFamily.addMember(helderPersonID);


        // Draw Money

        String drawMoneyDenomination2 = "Draw Money";
        CategoryID drawMoneyID2 = CategoryID.createCategoryID(drawMoneyDenomination, lemosFamilyID);
        lemosFamily.addCategory(drawMoneyID);

        // IRS

        String irsDenomination2 = "IRS";
        CategoryID irsID2 = CategoryID.createCategoryID(irsDenomination, lemosFamilyID);
        lemosFamily.addCategory(irsID);

        // Food

        String foodDenomination2 = "Food";
        CategoryID foodID2 = CategoryID.createCategoryID(foodDenomination, lemosFamilyID);
        lemosFamily.addCategory(foodID);


        // Water Bill

        String waterBillDenomination2 = "Water Bill";
        CategoryID waterBillID2 = CategoryID.createCategoryID(waterBillDenomination, lemosFamilyID);
        lemosFamily.addCategory(waterBillID);


        // Netflix

        String netflixDenomination2 = "Netflix";
        CategoryID netflixID2 = CategoryID.createCategoryID(netflixDenomination, lemosFamilyID);
        lemosFamily.addCategory(netflixID);
    }


    // Tests


    @Test
    @DisplayName("test for copyCategoryAsAdmin() | Person not in charge")
    void opyCategoryAsAdmin_FontesFamily_Success() {

        // Arrange
        String personEmail = "manuel@gmail.com";
        String groupDenomination1 = "Fontes Family";    // GroupFrom
        String groupDenomination2 = "Lemos Family";     // GroupTo

        String groupDescription1 = "All members from Fontes family";
        String groupDescription2 = "All members from Lemos family";

        LocalDate dateOfCreation1 = LocalDate.of(2020, 06, 01);
        LocalDate dateOfCreation2 = LocalDate.of(2020, 06, 01);

        String categoryDenomination = "Salary";

        // To Search
        CategoryID categoryID = CategoryID.createCategoryID(categoryDenomination, fontesFamilyID);

        // Returning an Optional<Group> Fontes Family
        Mockito.when(groupRepository.findById(fontesFamilyID)).thenReturn(Optional.of(fontesFamily));

        // Returning an Optional<Group> Lemos Family
        Mockito.when(groupRepository.findById(lemosFamilyID)).thenReturn(Optional.of(lemosFamily));

        // Returning False
        Mockito.when(categoryRepository.existsById(categoryID)).thenReturn(false);

        // DTO
        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO = CopyCategoryGroupToGroupDTOAssembler.createDTOFromPrimitiveTypes(
                personEmail, groupDenomination1, groupDenomination2, categoryDenomination);


        // Expected GroupDTO
        GroupDTO expectedGroupDTO = GroupDTOAssembler.createDTOFromDomainObject
                (Denomination.createDenomination(groupDenomination1),
                        Description.createDescription(groupDescription1),
                        DateOfCreation.createDateOfCreation(dateOfCreation1));


        // Service
        service = new US10CopyCategoryGroupToGroupService(groupRepository, categoryRepository);


        // Act
        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> service.copyCategoryAsAdmin(copyCategoryGroupToGroupDTO));


        // Assert
        assertEquals(thrown.getMessage(), US10CopyCategoryGroupToGroupService.PERSON_NOT_IN_CHARGE);
    }

}