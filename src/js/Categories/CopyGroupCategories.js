import React, {useContext, useEffect, useState} from "react";
import AppContext from "../context/AppContext";
import {
    Api,
    fetchCategoriesError,
    fetchCategoriesSuccess,
    fetch_group_success,
    fetch_group_error,
} from "../context/Actions";
import Button from "react-bootstrap/Button";

const CopyGroupCategories = () => {

        useEffect(() => {
            if (isLogged == true && !myPage && myGroups) {
                getGroupCategories();
                getGroups();
            }
        }, [])

        const {state, dispatch} = useContext(AppContext);

        const {isLogged, categories, userID, groupDenomination, myPage, myGroups, groups} = state;
        const {categoriesData} = categories;
        const {groupsData} = groups;

        const [category, setCategory] = useState('');
        const [group, setGroup] = useState('');


        const handleClick = (event) => {
            const user = userID.toLowerCase();
            console.log(user);
            event.preventDefault();
            console.log("handleChange: " + event.target);

            const request = {
                categoryDenomination: category,
            };


            if (!myPage && myGroups) {

                console.log(JSON.stringify(request));
                //dispach loading a true
                let data = Api.post('/persons/' + user + '/groups/' + groupDenomination + '/categories/groups/' + group,
                    {categoryDenomination: category})

                    .catch(err => alert(err.response.data.message + ';' + JSON.stringify(request)));

                setCategory('');
                setGroup('');

            }

            document.getElementById("myCategories").selectedIndex = "0";
            document.getElementById("myGroup").selectedIndex = "0";
        }


        const getGroups = async () => {

            const user = userID.toLowerCase();
            const url = '/persons/' + user + '/groups';

            try {
                const res = await Api.get(url);
                const {data} = await res;
                dispatch(fetch_group_success(data.groups));
                console.log(data.groups)
                console.log(JSON.stringify(data.groups))
                console.log("groups" + data.groups)
            } catch (err) {
                dispatch(fetch_group_error(err.message));
            }
        }

        const getGroupCategories = async () => {

            const user = userID.toLowerCase();

            const url = '/persons/' + user + '/groups/' + groupDenomination + '/categories';

            try {
                const res = await Api.get(url);
                const {data} = await res;
                dispatch(fetchCategoriesSuccess(data.categories));
            } catch (err) {
                dispatch(fetchCategoriesError(err.message));
            }
        }

        console.log(JSON.stringify(groupsData.map))
        console.log(categoriesData)

        return (
            <div>

                <form className="Forms" id="form" onSubmit={handleClick}>
                    <select id={"myCategories"} required
                            onChange={(event) => setCategory(event.target.value)}>
                        <option> Please enter Category</option>
                        {categoriesData && categoriesData.map((category) => {
                            return <option value={category}>{category}</option>
                        })}
                    </select>
                    <select id={"myGroup"} required
                            onChange={(event) => setGroup(event.target.value)}>
                        <option> Please enter group</option>
                        {groupsData && groupsData.map((group) => {
                            return <option value={group.denomination}>{group.denomination}</option>
                        })}
                    </select>

                    <Button variant="outline-primary" type="submit">Copy Category</Button>
                </form>
            </div>
        );
    }
;

export default CopyGroupCategories;