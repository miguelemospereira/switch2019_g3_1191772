package switch2019.project;

import switch2019.project.applicationLayer.applicationServices.CreatePersonService;
import switch2019.project.applicationLayer.applicationServices.US002_1CreateGroupService;
import switch2019.project.dtoLayer.dtos.*;
import switch2019.project.dtoLayer.dtosAssemblers.*;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Address;
import switch2019.project.domainLayer.domainEntities.vosShared.AccountID;
import switch2019.project.domainLayer.domainEntities.vosShared.CategoryID;
import switch2019.project.domainLayer.domainEntities.vosShared.GroupID;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * The type Bootstrapping.
 */
public class Bootstrapping {


    /*
    Person -> Siblings
        |
        |--Mother/Father
        |   |-> Paulo Fontes (paulo@gmail.com) / Helder Fontes (helder@gmail.com)
        |   |-> ...
        |
        |--Mother
        |   |-> Rui Silva (rui@gmail.com) / Henrique Bragança (henrique@gmail.com)
        |   |-> ...
        |
        |--Father
        |   |-> Rita Pereira (rita@gmail.com) / David Pereira (david@gmail.com)
        |   |-> ...
     */



    /*
    Group -> Families
        |
        |-> Fontes Family
        |-> Silva Family
        |-> Pereira Family
        |-> ...
     */

    /*
    Group -> Not Families
        |
        |-> Sunday Runners
        |-> ...
     */


    /**
     * Load data.
     *
     * @param createPersonService       the person services
     * @param us002_1CreateGroupService the group services
     */
    public static void loadData(CreatePersonService createPersonService, US002_1CreateGroupService us002_1CreateGroupService) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        //--------------------Addresses--------------------//


        //Porto

        String portoStreet = "Rua Direita do Viso";
        String portoDoorNumber = "59";
        String portoPostCode = "4250 - 198";
        String portoCity = "Porto";
        String portoCountry = "Portugal";

        Address porto = Address.createAddress(portoStreet, portoDoorNumber, portoPostCode, portoCity, portoCountry);

        //Braga

        String bragaStreet = "Praça Conde de Agrolongo";
        String bragaDoorNumber = "136";
        String bragaPostCode = "4700-312";
        String bragaCity = "Braga";
        String bragaCountry = "Portugal";

        Address braga = Address.createAddress(bragaStreet, bragaDoorNumber, bragaPostCode, bragaCity, bragaCountry);

        //Lisboa

        String lisboaStreet = "Av. José Malhoa";
        String lisboaDoorNumber = "12";
        String lisboaPostCode = "1099 - 017";
        String lisboaCity = "Lisboa";
        String lisboaCountry = "Portugal";

        Address lisboa = Address.createAddress(lisboaStreet, lisboaDoorNumber, lisboaPostCode, lisboaCity, lisboaCountry);


        //Porto -> morada joaquim@switch.pt

        String joaquimStreet = "Rua da Alegria";
        String joaquimDoorNumber = "5";
        String joaquimPostCode = "4250 - 198";
        String joaquimCity = "Porto";
        String joaquimCountry = "Portugal";

        //Coimbra -> morada maria@switch.pt

        String coimbraStreet = "Travessa Sol da Meia-Noite";
        String coimbraDoorNumber = "7";
        String coimbraPostCode = "1010 - 017";
        String coimbraCity = "Coimbra";
        String coimbraCountry = "Portugal";


        //--------------------Persons--------------------//


        //Others



        /*
        Person -> Alexandre Bragança (alexandre@gmail.com)
          |
          |--Mother ->      X
          |--Father ->      X
          |--Siblings ->    X
          |--Accounts ->    Company / Bank Account
          |--Categories ->  Salary
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String alexandreEmail = "alexandre@gmail.com";
        String alexandreName = "Alexandre Bragança";
        String alexandreBirthdate = LocalDate.of(1969, 10, 23).format(formatter);
        String alexandreBirthplace = "Vila Nova de Gaia";
        CreatePersonDTO createPersonDTO = CreatePersonDTOAssembler.createDTOFromPrimitiveTypes(alexandreEmail, alexandreName, alexandreBirthdate, alexandreBirthplace);

        PersonID alexandrePersonID = PersonID.createPersonID(alexandreEmail);
        createPersonService.createAndSavePerson(createPersonDTO);
        createPersonService.addAddressToPerson(alexandrePersonID, porto);

        //Accounts Alexandre Bragança
        //Company

        String alexandreCompanyDenomination = "Company";
        String alexandreCompanyDescription = "Company account";

//        Create CreatePersonAccountDTO
        CreatePersonAccountDTO createPersonCompanyAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(alexandreEmail, alexandreCompanyDescription, alexandreCompanyDenomination);


        createPersonService.addAccountToPerson(createPersonCompanyAccountDTO);
        AccountID alexandreCompanyAccountID = AccountID.createAccountID(alexandreCompanyDenomination, alexandrePersonID);

        //Bank Account

        String alexandreBankAccountDenomination = "Bank Account";
        String alexandreBankAccountDescription = "Personal bank account";

//        Create CreatePersonAccountDTO
        CreatePersonAccountDTO createPersonBankAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(alexandreEmail, alexandreBankAccountDescription, alexandreBankAccountDenomination);


        createPersonService.addAccountToPerson(createPersonBankAccountDTO);
        AccountID alexandreBankAccountID = AccountID.createAccountID(alexandreBankAccountDenomination, alexandrePersonID);

        //Categories Alexandre Bragança
        //Salary

        String alexandreSalaryDenomination = "Salary";

//        Create AddCategoryDTO
        CreatePersonCategoryDTO addCategoryDTO = CreatePersonCategoryDTOAssembler.createDTOFromPrimitiveTypes(alexandreEmail, alexandreSalaryDenomination);

        createPersonService.addCategoryToPerson(addCategoryDTO);
        CategoryID alexandreSalaryCategoryID = CategoryID.createCategoryID(alexandreSalaryDenomination, alexandrePersonID);

        //Transactions Alexandre Bragança

        //Salary January

        String alexandreCredit = "credit";
        String alexandreSalaryJanuaryDescription = "January salary";
        double alexandreSalaryJanuaryAmount = 1500;
        String alexandreSalaryJanuaryDate = "2020-01-21";

//        Create a Person Transaction DTO
        CreatePersonTransactionDTO createAlexandreTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(alexandreEmail,
                alexandreSalaryDenomination, alexandreCredit, alexandreSalaryJanuaryDescription, alexandreSalaryJanuaryAmount, alexandreCompanyDenomination, alexandreBankAccountDenomination, alexandreSalaryJanuaryDate);

        createPersonService.addPersonTransaction(createAlexandreTransactionDTO);



        /*
        Person -> Elsa Correia (elsa@gmail.com)
          |
          |--Mother ->      X
          |--Father ->      X
          |--Siblings ->    X
          |--Accounts ->    Company / Bank Account / Wallet / State / Supermarket / Household Expenses / Streaming Services
          |--Categories ->  Salary / Draw Money / IRS / Food / Water Bill / Netflix
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
                |-> 2020-01-25 / CREDIT / Draw Money / Bank Account -> Wallet / 100
                |-> 2020-01-27 / DEBIT / IRS / Bank Account -> State / 150
                |-> 2020-01-29 / DEBIT / Food / Wallet -> Supermaket / 50
                |-> 2020-01-30 / DEBIT / Water Bill / Bank Account -> Household Expenses / 50
                |-> 2020-01-30 / DEBIT / Netflix / Bank Account -> Streaming Services / 25
                |-> 2020-02-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String elsaEmail = "elsa@gmail.com";
        String elsaName = "Elsa Correia";
        String elsaBirthdate = LocalDate.of(1972, 12, 23).format(formatter);
        String elsaBirthplace = "Lisboa";
        CreatePersonDTO createElsaDTO = CreatePersonDTOAssembler.createDTOFromPrimitiveTypes(elsaEmail, elsaName, elsaBirthdate, elsaBirthplace);


        PersonID elsaPersonID = PersonID.createPersonID(elsaEmail);
        createPersonService.createAndSavePerson(createElsaDTO);
        createPersonService.addAddressToPerson(elsaPersonID, lisboa);


        createPersonAccounts(elsaPersonID, createPersonService);
        createPersonCategories(elsaPersonID, createPersonService);
        createPersonTransactions(elsaPersonID, createPersonService);


        //Fontes



        /*
        Person -> Ilda Fontes (ilda@gmail.com)
          |
          |--Mother ->      X
          |--Father ->      X
          |--Siblings ->    X
          |--Accounts ->    Company / Bank Account / Wallet / State / Supermarket / Household Expenses / Streaming Services
          |--Categories ->  Salary / Draw Money / IRS / Food / Water Bill / Netflix
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
                |-> 2020-01-25 / CREDIT / Draw Money / Bank Account -> Wallet / 100
                |-> 2020-01-27 / DEBIT / IRS / Bank Account -> State / 150
                |-> 2020-01-29 / DEBIT / Food / Wallet -> Supermaket / 50
                |-> 2020-01-30 / DEBIT / Water Bill / Bank Account -> Household Expenses / 50
                |-> 2020-01-30 / DEBIT / Netflix / Bank Account -> Streaming Services / 25
                |-> 2020-02-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String ildaEmail = "ilda@gmail.com";
        String ildaName = "Ilda Fontes";
        String ildaBirthdate = LocalDate.of(1963, 01, 9).format(formatter);
        String ildaBirthplace = "Vila Nova de Gaia";
        CreatePersonDTO createIldaDTO = CreatePersonDTOAssembler.createDTOFromPrimitiveTypes(ildaEmail, ildaName, ildaBirthdate, ildaBirthplace);


        PersonID ildaPersonID = PersonID.createPersonID(ildaEmail);
        createPersonService.createAndSavePerson(createIldaDTO);
        createPersonService.addAddressToPerson(ildaPersonID, porto);

        createPersonAccounts(ildaPersonID, createPersonService);
        createPersonCategories(ildaPersonID, createPersonService);
        createPersonTransactions(ildaPersonID, createPersonService);


        /*
        Person -> Manuel Fontes (manuel@gmail.com)
          |
          |--Mother ->      X
          |--Father ->      X
          |--Siblings ->    X
          |--Accounts ->    Company / Bank Account / Wallet / State / Supermarket / Household Expenses / Streaming Services
          |--Categories ->  Salary / Draw Money / IRS / Food / Water Bill / Netflix
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
                |-> 2020-01-25 / CREDIT / Draw Money / Bank Account -> Wallet / 100
                |-> 2020-01-27 / DEBIT / IRS / Bank Account -> State / 150
                |-> 2020-01-29 / DEBIT / Food / Wallet -> Supermaket / 50
                |-> 2020-01-30 / DEBIT / Water Bill / Bank Account -> Household Expenses / 50
                |-> 2020-01-30 / DEBIT / Netflix / Bank Account -> Streaming Services / 25
                |-> 2020-02-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String manuelEmail = "manuel@gmail.com";
        String manuelName = "Manuel Fontes";
        String manuelBirthdate = LocalDate.of(1964, 01, 16).format(formatter);
        String manuelBirthplace = "Vila Nova de Gaia";
        CreatePersonDTO createmanuelDTO = CreatePersonDTOAssembler.createDTOFromPrimitiveTypes(manuelEmail, manuelName, manuelBirthdate, manuelBirthplace);


        PersonID manuelPersonID = PersonID.createPersonID(manuelEmail);
        createPersonService.createAndSavePerson(createmanuelDTO);
        createPersonService.addAddressToPerson(manuelPersonID, porto);

        createPersonAccounts(manuelPersonID, createPersonService);
        createPersonCategories(manuelPersonID, createPersonService);
        createPersonTransactions(manuelPersonID, createPersonService);


        /*
        Person -> Paulo Fontes (paulo@gmail.com)
          |
          |--Mother ->      Ilda Fontes (ilda@gmail.com)
          |--Father ->      Manuel Fontes (manuel@gmail.com)
          |--Siblings ->    Helder Fontes (helder@gmail.com)
          |--Accounts ->    Company / Bank Account / Wallet / State / Supermarket / Household Expenses / Streaming Services
          |--Categories ->  Salary / Draw Money / IRS / Food / Water Bill / Netflix
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
                |-> 2020-01-25 / CREDIT / Draw Money / Bank Account -> Wallet / 100
                |-> 2020-01-27 / DEBIT / IRS / Bank Account -> State / 150
                |-> 2020-01-29 / DEBIT / Food / Wallet -> Supermaket / 50
                |-> 2020-01-30 / DEBIT / Water Bill / Bank Account -> Household Expenses / 50
                |-> 2020-01-30 / DEBIT / Netflix / Bank Account -> Streaming Services / 25
                |-> 2020-02-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String pauloEmail = "paulo@gmail.com";
        String pauloName = "Paulo Fontes";
        String pauloBirthdate = LocalDate.of(1993, 03, 15).format(formatter);
        String pauloBirthplace = "Vila Nova de Gaia";
        CreatePersonDTO createpauloDTO = CreatePersonDTOAssembler.createDTOFromPrimitiveTypes(pauloEmail, pauloName, pauloBirthdate, pauloBirthplace);


        PersonID pauloPersonID = PersonID.createPersonID(pauloEmail);
        createPersonService.createAndSavePerson(createpauloDTO);
        createPersonService.addAddressToPerson(pauloPersonID, porto);
        createPersonService.addFatherToPerson(pauloPersonID, manuelPersonID);
        createPersonService.addMotherToPerson(pauloPersonID, ildaPersonID);

        createPersonAccounts(pauloPersonID, createPersonService);
        createPersonCategories(pauloPersonID, createPersonService);
        createPersonTransactions(pauloPersonID, createPersonService);


        /*
        Person -> Helder Fontes (helder@gmail.com)
          |
          |--Mother ->      Ilda Fontes (ilda@gmail.com)
          |--Father ->      Manuel Fontes (manuel@gmail.com)
          |--Siblings ->    Paulo Fontes (paulo@gmail.com)
          |--Accounts ->    Company / Bank Account / Wallet / State / Supermarket / Household Expenses / Streaming Services
          |--Categories ->  Salary / Draw Money / IRS / Food / Water Bill / Netflix
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
                |-> 2020-01-25 / CREDIT / Draw Money / Bank Account -> Wallet / 100
                |-> 2020-01-27 / DEBIT / IRS / Bank Account -> State / 150
                |-> 2020-01-29 / DEBIT / Food / Wallet -> Supermaket / 50
                |-> 2020-01-30 / DEBIT / Water Bill / Bank Account -> Household Expenses / 50
                |-> 2020-01-30 / DEBIT / Netflix / Bank Account -> Streaming Services / 25
                |-> 2020-02-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String helderEmail = "helder@gmail.com";
        String helderName = "Helder Fontes";
        String helderBirthdate = LocalDate.of(1983, 01, 30).format(formatter);
        String helderBirthplace = "Vila Nova de Gaia";
        CreatePersonDTO createhelderDTO = CreatePersonDTOAssembler.createDTOFromPrimitiveTypes(helderEmail, helderName, helderBirthdate, helderBirthplace);


        PersonID helderPersonID = PersonID.createPersonID(helderEmail);
        createPersonService.createAndSavePerson(createhelderDTO);
        createPersonService.addAddressToPerson(helderPersonID, porto);
        createPersonService.addFatherToPerson(helderPersonID, manuelPersonID);
        createPersonService.addMotherToPerson(helderPersonID, ildaPersonID);

        createPersonAccounts(helderPersonID, createPersonService);
        createPersonCategories(helderPersonID, createPersonService);
        createPersonTransactions(helderPersonID, createPersonService);


        //Silva



        /*
        Person -> João Silva (joao@gmail.com)
          |
          |--Mother ->      X
          |--Father ->      X
          |--Siblings ->    X
          |--Accounts ->    Company / Bank Account / Wallet / State / Supermarket / Household Expenses / Streaming Services
          |--Categories ->  Salary / Draw Money / IRS / Food / Water Bill / Netflix
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
                |-> 2020-01-25 / CREDIT / Draw Money / Bank Account -> Wallet / 100
                |-> 2020-01-27 / DEBIT / IRS / Bank Account -> State / 150
                |-> 2020-01-29 / DEBIT / Food / Wallet -> Supermaket / 50
                |-> 2020-01-30 / DEBIT / Water Bill / Bank Account -> Household Expenses / 50
                |-> 2020-01-30 / DEBIT / Netflix / Bank Account -> Streaming Services / 25
                |-> 2020-02-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String joaoEmail = "joao@gmail.com";
        String joaoName = "João Silva";
        String joaoBirthdate = LocalDate.of(1975, 04, 13).format(formatter);
        String joaoBirthplace = "Braga";
        CreatePersonDTO createjoaoDTO = CreatePersonDTOAssembler.createDTOFromPrimitiveTypes(joaoEmail, joaoName, joaoBirthdate, joaoBirthplace);


        PersonID joaoPersonID = PersonID.createPersonID(joaoEmail);
        createPersonService.createAndSavePerson(createjoaoDTO);
        createPersonService.addAddressToPerson(joaoPersonID, braga);

        createPersonAccounts(joaoPersonID, createPersonService);
        createPersonCategories(joaoPersonID, createPersonService);
        createPersonTransactions(joaoPersonID, createPersonService);


        /*
        Person -> Maria Silva (maria@gmail.com)
          |
          |--Mother ->      X
          |--Father ->      X
          |--Siblings ->    X
          |--Accounts ->    Company / Bank Account / Wallet / State / Supermarket / Household Expenses / Streaming Services
          |--Categories ->  Salary / Draw Money / IRS / Food / Water Bill / Netflix
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
                |-> 2020-01-25 / CREDIT / Draw Money / Bank Account -> Wallet / 100
                |-> 2020-01-27 / DEBIT / IRS / Bank Account -> State / 150
                |-> 2020-01-29 / DEBIT / Food / Wallet -> Supermaket / 50
                |-> 2020-01-30 / DEBIT / Water Bill / Bank Account -> Household Expenses / 50
                |-> 2020-01-30 / DEBIT / Netflix / Bank Account -> Streaming Services / 25
                |-> 2020-02-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String mariaEmail = "maria@gmail.com";
        String mariaName = "Maria Silva";
        String mariaBirthdate = LocalDate.of(1973, 07, 25).format(formatter);
        String mariaBirthplace = "Braga";
        CreatePersonDTO createmariaDTO = CreatePersonDTOAssembler.createDTOFromPrimitiveTypes(mariaEmail, mariaName, mariaBirthdate, mariaBirthplace);


        PersonID mariaPersonID = PersonID.createPersonID(mariaEmail);
        createPersonService.createAndSavePerson(createmariaDTO);
        createPersonService.addAddressToPerson(mariaPersonID, braga);

        createPersonAccounts(mariaPersonID, createPersonService);
        createPersonCategories(mariaPersonID, createPersonService);
        createPersonTransactions(mariaPersonID, createPersonService);


        /*
        Person -> Rui Silva (paulo@gmail.com)
          |
          |--Mother ->      Maria Silva (maria@gmail.com)
          |--Father ->      João Silva (joao@gmail.com)
          |--Siblings ->    Henrique Bragança (henrique@gmail.com)
          |--Accounts ->    Company / Bank Account / Wallet / State / Supermarket / Household Expenses / Streaming Services
          |--Categories ->  Salary / Draw Money / IRS / Food / Water Bill / Netflix
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
                |-> 2020-01-25 / CREDIT / Draw Money / Bank Account -> Wallet / 100
                |-> 2020-01-27 / DEBIT / IRS / Bank Account -> State / 150
                |-> 2020-01-29 / DEBIT / Food / Wallet -> Supermaket / 50
                |-> 2020-01-30 / DEBIT / Water Bill / Bank Account -> Household Expenses / 50
                |-> 2020-01-30 / DEBIT / Netflix / Bank Account -> Streaming Services / 25
                |-> 2020-02-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String ruiEmail = "rui@gmail.com";
        String ruiName = "Rui Silva";
        String ruiBirthdate = LocalDate.of(1991, 04, 02).format(formatter);
        String ruiBirthplace = "Braga";
        CreatePersonDTO createruiDTO = CreatePersonDTOAssembler.createDTOFromPrimitiveTypes(ruiEmail, ruiName, ruiBirthdate, ruiBirthplace);


        PersonID ruiPersonID = PersonID.createPersonID(ruiEmail);
        createPersonService.createAndSavePerson(createruiDTO);
        createPersonService.addAddressToPerson(ruiPersonID, braga);
        createPersonService.addFatherToPerson(ruiPersonID, joaoPersonID);
        createPersonService.addMotherToPerson(ruiPersonID, mariaPersonID);

        createPersonAccounts(ruiPersonID, createPersonService);
        createPersonCategories(ruiPersonID, createPersonService);
        createPersonTransactions(ruiPersonID, createPersonService);


        /*
        Person -> Henrique Bragança (henrique@gmail.com)
          |
          |--Mother ->      Maria Silva (maria@gmail.com)
          |--Father ->      Alexandre Bragança (alexandre@gmail.com)
          |--Siblings ->    Rui Silva (rui@gmail.com)
          |--Accounts ->    Company / Bank Account / Wallet / State / Supermarket / Household Expenses / Streaming Services
          |--Categories ->  Salary / Draw Money / IRS / Food / Water Bill / Netflix
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
                |-> 2020-01-25 / CREDIT / Draw Money / Bank Account -> Wallet / 100
                |-> 2020-01-27 / DEBIT / IRS / Bank Account -> State / 150
                |-> 2020-01-29 / DEBIT / Food / Wallet -> Supermaket / 50
                |-> 2020-01-30 / DEBIT / Water Bill / Bank Account -> Household Expenses / 50
                |-> 2020-01-30 / DEBIT / Netflix / Bank Account -> Streaming Services / 25
                |-> 2020-02-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String henriqueEmail = "henrique@gmail.com";
        String henriqueName = "Henrique Bragança";
        String henriqueBirthdate = LocalDate.of(1993, 04, 02).format(formatter);
        String henriqueBirthplace = "Braga";
        CreatePersonDTO createhenriqueDTO = CreatePersonDTOAssembler.createDTOFromPrimitiveTypes(henriqueEmail, henriqueName, henriqueBirthdate, henriqueBirthplace);


        PersonID henriquePersonID = PersonID.createPersonID(henriqueEmail);
        createPersonService.createAndSavePerson(createhenriqueDTO);
        createPersonService.addAddressToPerson(henriquePersonID, braga);
        createPersonService.addFatherToPerson(henriquePersonID, alexandrePersonID);
        createPersonService.addMotherToPerson(henriquePersonID, mariaPersonID);

        createPersonAccounts(henriquePersonID, createPersonService);
        createPersonCategories(henriquePersonID, createPersonService);
        createPersonTransactions(henriquePersonID, createPersonService);


        //Pereira



        /*
        Person -> Ricardo Pereira (ricardo@gmail.com)
          |
          |--Mother ->      X
          |--Father ->      X
          |--Siblings ->    X
          |--Accounts ->    Company / Bank Account / Wallet / State / Supermarket / Household Expenses / Streaming Services
          |--Categories ->  Salary / Draw Money / IRS / Food / Water Bill / Netflix
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
                |-> 2020-01-25 / CREDIT / Draw Money / Bank Account -> Wallet / 100
                |-> 2020-01-27 / DEBIT / IRS / Bank Account -> State / 150
                |-> 2020-01-29 / DEBIT / Food / Wallet -> Supermaket / 50
                |-> 2020-01-30 / DEBIT / Water Bill / Bank Account -> Household Expenses / 50
                |-> 2020-01-30 / DEBIT / Netflix / Bank Account -> Streaming Services / 25
                |-> 2020-02-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String ricardoEmail = "ricardo@gmail.com";
        String ricardoName = "Ricardo Pereira";
        String ricardoBirthdate = LocalDate.of(1975, 04, 13).format(formatter);
        String ricardoBirthplace = "Lisboa";
        CreatePersonDTO createRicardoDTO = CreatePersonDTOAssembler.createDTOFromPrimitiveTypes(ricardoEmail, ricardoName, ricardoBirthdate, ricardoBirthplace);


        PersonID ricardoPersonID = PersonID.createPersonID(ricardoEmail);
        createPersonService.createAndSavePerson(createRicardoDTO);
        createPersonService.addAddressToPerson(ricardoPersonID, lisboa);

        createPersonAccounts(ricardoPersonID, createPersonService);
        createPersonCategories(ricardoPersonID, createPersonService);
        createPersonTransactions(ricardoPersonID, createPersonService);


        /*
        Person -> Ana Pereira (ana@gmail.com)
          |
          |--Mother ->      X
          |--Father ->      X
          |--Siblings ->    X
          |--Accounts ->    Company / Bank Account / Wallet / State / Supermarket / Household Expenses / Streaming Services
          |--Categories ->  Salary / Draw Money / IRS / Food / Water Bill / Netflix
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
                |-> 2020-01-25 / CREDIT / Draw Money / Bank Account -> Wallet / 100
                |-> 2020-01-27 / DEBIT / IRS / Bank Account -> State / 150
                |-> 2020-01-29 / DEBIT / Food / Wallet -> Supermaket / 50
                |-> 2020-01-30 / DEBIT / Water Bill / Bank Account -> Household Expenses / 50
                |-> 2020-01-30 / DEBIT / Netflix / Bank Account -> Streaming Services / 25
                |-> 2020-02-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String anaEmail = "ana@gmail.com";
        String anaName = "Ana Pereira";
        String anaBirthdate = LocalDate.of(1975, 07, 25).format(formatter);
        String anaBirthplace = "Lisboa";
        CreatePersonDTO createAnaDTO = CreatePersonDTOAssembler.createDTOFromPrimitiveTypes(anaEmail, anaName, anaBirthdate, anaBirthplace);


        PersonID anaPersonID = PersonID.createPersonID(anaEmail);
        createPersonService.createAndSavePerson(createAnaDTO);
        createPersonService.addAddressToPerson(anaPersonID, lisboa);

        createPersonAccounts(anaPersonID, createPersonService);
        createPersonCategories(anaPersonID, createPersonService);
        createPersonTransactions(anaPersonID, createPersonService);


        /*
        Person -> Rita Pereira (rita@gmail.com)
          |
          |--Mother ->      Ana Pereira (ana@gmail.com)
          |--Father ->      Ricardo Pereira (ricardo@gmail.com)
          |--Siblings ->    David Pereira (david@gmail.com)
          |--Accounts ->    Company / Bank Account / Wallet / State / Supermarket / Household Expenses / Streaming Services
          |--Categories ->  Salary / Draw Money / IRS / Food / Water Bill / Netflix
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
                |-> 2020-01-25 / CREDIT / Draw Money / Bank Account -> Wallet / 100
                |-> 2020-01-27 / DEBIT / IRS / Bank Account -> State / 150
                |-> 2020-01-29 / DEBIT / Food / Wallet -> Supermaket / 50
                |-> 2020-01-30 / DEBIT / Water Bill / Bank Account -> Household Expenses / 50
                |-> 2020-01-30 / DEBIT / Netflix / Bank Account -> Streaming Services / 25
                |-> 2020-02-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String ritaEmail = "rita@gmail.com";
        String ritaName = "Rita Pereira";
        String ritaBirthdate = LocalDate.of(1994, 06, 22).format(formatter);
        String ritaBirthplace = "Lisboa";
        CreatePersonDTO createRitaDTO = CreatePersonDTOAssembler.createDTOFromPrimitiveTypes(ritaEmail, ritaName, ritaBirthdate, ritaBirthplace);


        PersonID ritaPersonID = PersonID.createPersonID(ritaEmail);
        createPersonService.createAndSavePerson(createRitaDTO);
        createPersonService.addAddressToPerson(ritaPersonID, lisboa);
        createPersonService.addFatherToPerson(ritaPersonID, ricardoPersonID);
        createPersonService.addMotherToPerson(ritaPersonID, anaPersonID);

        createPersonAccounts(ritaPersonID, createPersonService);
        createPersonCategories(ritaPersonID, createPersonService);
        createPersonTransactions(ritaPersonID, createPersonService);


        /*
        Person -> David Pereira (david@gmail.com)
          |
          |--Mother ->      Elsa Correia (elsa@gmail.com)
          |--Father ->      Ricardo Pereira (ricardo@gmail.com)
          |--Siblings ->    Rita Pereira (rita@gmail.com)
          |--Accounts ->    Company / Bank Account / Wallet / State / Supermarket / Household Expenses / Streaming Services
          |--Categories ->  Salary / Draw Money / IRS / Food / Water Bill / Netflix
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
                |-> 2020-01-25 / CREDIT / Draw Money / Bank Account -> Wallet / 100
                |-> 2020-01-27 / DEBIT / IRS / Bank Account -> State / 150
                |-> 2020-01-29 / DEBIT / Food / Wallet -> Supermaket / 50
                |-> 2020-01-30 / DEBIT / Water Bill / Bank Account -> Household Expenses / 50
                |-> 2020-01-30 / DEBIT / Netflix / Bank Account -> Streaming Services / 25
                |-> 2020-02-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String davidEmail = "david@gmail.com";
        String davidName = "David Pereira";
        String davidBirthdate = LocalDate.of(1993, 9, 26).format(formatter);
        String davidBirthplace = "Lisboa";
        CreatePersonDTO createDavidDTO = CreatePersonDTOAssembler.createDTOFromPrimitiveTypes(davidEmail, davidName, davidBirthdate, davidBirthplace);


        PersonID davidPersonID = PersonID.createPersonID(davidEmail);
        createPersonService.createAndSavePerson(createDavidDTO);
        createPersonService.addAddressToPerson(davidPersonID, lisboa);
        createPersonService.addFatherToPerson(davidPersonID, ricardoPersonID);
        createPersonService.addMotherToPerson(davidPersonID, elsaPersonID);

        createPersonAccounts(davidPersonID, createPersonService);
        createPersonCategories(davidPersonID, createPersonService);
        createPersonTransactions(davidPersonID, createPersonService);

        /*
        Person -> Joaquim (joaquim@switch.pt)
          |
          |--Mother ->      X
          |--Father ->      X
          |--Siblings ->    X
          |--Accounts ->    Company / Bank Account
          |--Categories ->  Salary
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String joaquimEmail = "joaquim@switch.pt";
        String joaquimName = "Joaquim";
        String joaquimBirthdate = LocalDate.of(1991, 12, 25).format(formatter);
        String joaquimBirthplace = "Porto";
        CreatePersonDTO createJoaquimDTO = CreatePersonDTOAssembler.createDTOFromPrimitiveTypes(joaquimEmail, joaquimName, joaquimBirthdate, joaquimBirthplace);

        PersonID joaquimPersonID = PersonID.createPersonID(joaquimEmail);
        createPersonService.createAndSavePerson(createJoaquimDTO);
        createPersonService.addAddressToPerson(joaquimPersonID, porto);

        //Accounts Joaquim
        //Revolut

        String revolutDenomination = "Revolut";
        String revolutDescription = "Cartão";

//        Create CreatePersonAccountDTO
        CreatePersonAccountDTO revolutAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(joaquimEmail, revolutDescription, revolutDenomination);


        createPersonService.addAccountToPerson(revolutAccountDTO);
        AccountID revolutAccountID = AccountID.createAccountID(revolutDenomination, joaquimPersonID);

        //Mercearia

        String merceariaAccountDenomination = "Mercearia";
        String merceariaAccountDescription = "Mercearia do Bairro";

//        Create CreatePersonAccountDTO
        CreatePersonAccountDTO merceariaAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(joaquimEmail, merceariaAccountDescription, merceariaAccountDenomination);


        createPersonService.addAccountToPerson(merceariaAccountDTO);
        AccountID merceariaAccountID = AccountID.createAccountID(merceariaAccountDenomination, joaquimPersonID);

        //Netflix

        String netflixAccountDenomination = "Netflix";
        String netflixAccountDescription = "Netflix";

//        Create CreatePersonAccountDTO
        CreatePersonAccountDTO netflixAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(joaquimEmail, netflixAccountDescription, netflixAccountDenomination);


        createPersonService.addAccountToPerson(netflixAccountDTO);
        AccountID netflixAccountID = AccountID.createAccountID(netflixAccountDenomination, joaquimPersonID);


        //Categories Joaquim
        //Food

        String foodCategoryDenomination = "Food";

//        Create AddCategoryDTO
        CreatePersonCategoryDTO foodCategoryDTO = CreatePersonCategoryDTOAssembler.createDTOFromPrimitiveTypes(joaquimEmail, foodCategoryDenomination);

        createPersonService.addCategoryToPerson(foodCategoryDTO);
        CategoryID foodCategoryID = CategoryID.createCategoryID(foodCategoryDenomination, joaquimPersonID);

        //Streaming Services

        String streamingServicesCategoryDenomination = "Streaming Services";

//        Create AddCategoryDTO
        CreatePersonCategoryDTO streamingServicesCategoryDTO = CreatePersonCategoryDTOAssembler.createDTOFromPrimitiveTypes(joaquimEmail, streamingServicesCategoryDenomination);

        createPersonService.addCategoryToPerson(streamingServicesCategoryDTO);
        CategoryID streamingServicesCategoryID = CategoryID.createCategoryID(streamingServicesCategoryDenomination, joaquimPersonID);

        //Transactions Joaquim

        //July Groceries

        String julyGroceriesDebit = "debit";
        String julyGroceriesDescription = "July Groceries";
        double julyGroceriesAmount = 75;
        String julyGroceriesDate = "2019-07-23";

//        Create a July Groceries Transaction DTO
        CreatePersonTransactionDTO julyGroceriesTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(joaquimEmail,
                foodCategoryDenomination, julyGroceriesDebit, julyGroceriesDescription, julyGroceriesAmount, revolutDenomination, merceariaAccountDenomination, julyGroceriesDate);

        createPersonService.addPersonTransaction(julyGroceriesTransactionDTO);

        //August Groceries

        String augustGroceriesDebit = "debit";
        String augustGroceriesDescription = "August Groceries";
        double augustGroceriesAmount = 90;
        String augustGroceriesDate = "2019-08-21";

//        Create a July Groceries Transaction DTO
        CreatePersonTransactionDTO augustGroceriesTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(joaquimEmail,
                foodCategoryDenomination, augustGroceriesDebit, augustGroceriesDescription, augustGroceriesAmount, revolutDenomination, merceariaAccountDenomination, augustGroceriesDate);

        createPersonService.addPersonTransaction(augustGroceriesTransactionDTO);

        //September Netflix

        String septemberNetflixDebit = "debit";
        String septemberNetflixDescription = "September Netflix";
        double septemberNetflixAmount = 15;
        String septemberNetflixDate = "2019-08-21";

//        Create a July Groceries Transaction DTO
        CreatePersonTransactionDTO septemberNetflixTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(joaquimEmail,
                foodCategoryDenomination, septemberNetflixDebit, septemberNetflixDescription, septemberNetflixAmount, revolutDenomination, netflixAccountDenomination, septemberNetflixDate);

        createPersonService.addPersonTransaction(septemberNetflixTransactionDTO);



        /*
        Person -> Joaquim (joaquim@switch.pt)
          |
          |--Mother ->      X
          |--Father ->      X
          |--Siblings ->    X
          |--Accounts ->    Company / Bank Account
          |--Categories ->  Salary
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String mariaSwitchEmail = "maria@switch.pt";
        String mariaSwitchName = "Maria";
        String mariaSwitchBirthdate = LocalDate.of(1990, 04, 01).format(formatter);
        String mariaSwitchBirthplace = "Coimbra";
        CreatePersonDTO createMariaSwitchDTO = CreatePersonDTOAssembler.createDTOFromPrimitiveTypes(mariaSwitchEmail, mariaSwitchName, mariaSwitchBirthdate, mariaSwitchBirthplace);

        PersonID mariaSwitchPersonID = PersonID.createPersonID(mariaSwitchEmail);
        createPersonService.createAndSavePerson(createMariaSwitchDTO);
        createPersonService.addAddressToPerson(mariaSwitchPersonID, porto);


        //--------------------Groups--------------------//



        /*
        Group -> Fontes Family
          |
          |--peopleInCharge ->      Manuel Fontes (manuel@gmail.com) / Ilda Fontes (ilda@gmail.com)
          |--members ->             Paulo Fontes (paulo@gmail.com) / Helder Fontes (helder@gmail.com)
          |--Accounts ->            Company / Bank Account / Wallet / State / Supermarket / Household Expenses / Streaming Services
          |--Categories ->          Salary / Draw Money / IRS / Food / Water Bill / Netflix
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
                |-> 2020-01-25 / CREDIT / Draw Money / Bank Account -> Wallet / 100
                |-> 2020-01-27 / DEBIT / IRS / Bank Account -> State / 150
                |-> 2020-01-29 / DEBIT / Food / Wallet -> Supermaket / 50
                |-> 2020-01-30 / DEBIT / Water Bill / Bank Account -> Household Expenses / 50
                |-> 2020-01-30 / DEBIT / Netflix / Bank Account -> Streaming Services / 25
                |-> 2020-02-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String fontesFamilyDenomination = "Fontes Family";
        String fontesFamilyDescription = "All members from Fontes family";

        GroupID fontesFamilyID = GroupID.createGroupID(fontesFamilyDenomination);
        us002_1CreateGroupService.createAndSaveGroup(fontesFamilyDenomination, fontesFamilyDescription, manuelPersonID);

        us002_1CreateGroupService.addAdminToGroup(fontesFamilyID, ildaPersonID);
        us002_1CreateGroupService.addMemberToGroup(fontesFamilyID, pauloPersonID);
        us002_1CreateGroupService.addMemberToGroup(fontesFamilyID, helderPersonID);

        createGroupAccounts(fontesFamilyID, us002_1CreateGroupService);
        createGroupCategories(fontesFamilyID, us002_1CreateGroupService);
        createGroupTransactions(fontesFamilyID, us002_1CreateGroupService);


        /*
        Group -> Silva Family
          |
          |--peopleInCharge ->      João Silva (joao@gmail.com) / Maria Silva (maria@gmail.com)
          |--members ->             Rui Silva (paulo@gmail.com) / Henrique Bragança (henrique@gmail.com)
          |--Accounts ->            Company / Bank Account / Wallet / State / Supermarket / Household Expenses / Streaming Services
          |--Categories ->          Salary / Draw Money / IRS / Food / Water Bill / Netflix
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
                |-> 2020-01-25 / CREDIT / Draw Money / Bank Account -> Wallet / 100
                |-> 2020-01-27 / DEBIT / IRS / Bank Account -> State / 150
                |-> 2020-01-29 / DEBIT / Food / Wallet -> Supermaket / 50
                |-> 2020-01-30 / DEBIT / Water Bill / Bank Account -> Household Expenses / 50
                |-> 2020-01-30 / DEBIT / Netflix / Bank Account -> Streaming Services / 25
                |-> 2020-02-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String silvaFamilyDenomination = "Silva Family";
        String silvaFamilyDescription = "All members from Silva family";

        GroupID silvaFamilyID = GroupID.createGroupID(silvaFamilyDenomination);
        us002_1CreateGroupService.createAndSaveGroup(silvaFamilyDenomination, silvaFamilyDescription, joaoPersonID);

        us002_1CreateGroupService.addAdminToGroup(silvaFamilyID, mariaPersonID);
        us002_1CreateGroupService.addMemberToGroup(silvaFamilyID, ruiPersonID);
        us002_1CreateGroupService.addMemberToGroup(silvaFamilyID, henriquePersonID);

        createGroupAccounts(silvaFamilyID, us002_1CreateGroupService);
        createGroupCategories(silvaFamilyID, us002_1CreateGroupService);
        createGroupTransactions(silvaFamilyID, us002_1CreateGroupService);

        /*
        Group -> Pereira Family
          |
          |--peopleInCharge ->      Ricardo Pereira (ricardo@gmail.com) / Ana Pereira (ana@gmail.com)
          |--members ->             Rita Pereira (rita@gmail.com) / David Pereira (david@gmail.com)
          |--Accounts ->            Company / Bank Account / Wallet / State / Supermarket / Household Expenses / Streaming Services
          |--Categories ->          Salary / Draw Money / IRS / Food / Water Bill / Netflix
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
                |-> 2020-01-25 / CREDIT / Draw Money / Bank Account -> Wallet / 100
                |-> 2020-01-27 / DEBIT / IRS / Bank Account -> State / 150
                |-> 2020-01-29 / DEBIT / Food / Wallet -> Supermaket / 50
                |-> 2020-01-30 / DEBIT / Water Bill / Bank Account -> Household Expenses / 50
                |-> 2020-01-30 / DEBIT / Netflix / Bank Account -> Streaming Services / 25
                |-> 2020-02-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String pereiraFamilyDenomination = "Pereira Family";
        String pereiraFamilyDescription = "All members from Pereira family";

        GroupID pereiraFamilyID = GroupID.createGroupID(pereiraFamilyDenomination);
        us002_1CreateGroupService.createAndSaveGroup(pereiraFamilyDenomination, pereiraFamilyDescription, ricardoPersonID);

        us002_1CreateGroupService.addAdminToGroup(pereiraFamilyID, anaPersonID);
        us002_1CreateGroupService.addMemberToGroup(pereiraFamilyID, ritaPersonID);
        us002_1CreateGroupService.addMemberToGroup(pereiraFamilyID, davidPersonID);

        createGroupAccounts(pereiraFamilyID, us002_1CreateGroupService);
        createGroupCategories(pereiraFamilyID, us002_1CreateGroupService);
        createGroupTransactions(pereiraFamilyID, us002_1CreateGroupService);


        /*
        Group -> Sunday Runners
          |
          |--peopleInCharge ->      Paulo Fontes (paulo@gmail.com)
          |--members ->             Rita Pereira (rita@gmail.com) / Henrique Bragança (henrique@gmail.com)
          |--Accounts ->            Company / Bank Account
          |--Categories ->          Salary
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String sundayRunnersDenomination = "Sunday Runners";
        String sundayRunnersDescription = "All members from Sunday Runners";

        GroupID sundayRunnersID = GroupID.createGroupID(sundayRunnersDenomination);
        us002_1CreateGroupService.createAndSaveGroup(sundayRunnersDenomination, sundayRunnersDescription, pauloPersonID);

        us002_1CreateGroupService.addMemberToGroup(sundayRunnersID, ritaPersonID);
        us002_1CreateGroupService.addMemberToGroup(sundayRunnersID, henriquePersonID);


        //Accounts Sunday Runners
        //Company

        String companyDenomination = "Company";
        String companyDescription = "Company account";

        us002_1CreateGroupService.addAccountToGroup(sundayRunnersID, companyDenomination, companyDescription);
        AccountID companyAccountID = AccountID.createAccountID(companyDenomination, sundayRunnersID);

        //Bank Account

        String bankAccountDenomination = "Bank Account";
        String bankAccountDescription = "Personal bank account";

        us002_1CreateGroupService.addAccountToGroup(sundayRunnersID, bankAccountDenomination, bankAccountDescription);
        AccountID bankAccountID = AccountID.createAccountID(bankAccountDenomination, sundayRunnersID);

        //Categories Sunday Runners
        //Salary

        String salaryDenomination = "Salary";

        us002_1CreateGroupService.addCategoryToGroup(sundayRunnersID, salaryDenomination);
        CategoryID salaryCategoryID = CategoryID.createCategoryID(salaryDenomination, sundayRunnersID);

        //Transactions Sunday Runners

        //Salary January

        String credit = "credit";
        String salaryJanuaryDescription = "January salary";
        double salaryJanuaryAmount = 1500;
        LocalDate salaryJanuaryDate = LocalDate.of(2020, 01, 21);
        us002_1CreateGroupService.addGroupTransaction(sundayRunnersID, salaryCategoryID, credit, salaryJanuaryDescription, salaryJanuaryAmount, salaryJanuaryDate, companyAccountID, bankAccountID);



            /*
        Group -> Students
          |
          |--peopleInCharge ->      Joaquim (joaquim@switch.pt)
          |--members ->             Maria (maria@switch.pt)
          |--Accounts ->            Wallet / Register
          |--Categories ->          Groceries / Tuition
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */

        String studentsDenomination = "Students";
        String studentsDescription = "Students";

        GroupID studentsID = GroupID.createGroupID(studentsDenomination);
        us002_1CreateGroupService.createAndSaveGroup(studentsDenomination, studentsDescription, joaquimPersonID);

        us002_1CreateGroupService.addMemberToGroup(studentsID, mariaSwitchPersonID);

        //Accounts Students
        //Wallet Account

        String walletDenomination = "Wallet";
        String walletDescription = "My wallet";

        us002_1CreateGroupService.addAccountToGroup(studentsID, walletDenomination, walletDescription);
        AccountID walletAccountID = AccountID.createAccountID(walletDenomination, studentsID);

        //Register Account

        String registerDenomination = "Register";
        String registerDescription = "My register";

        us002_1CreateGroupService.addAccountToGroup(studentsID, registerDenomination, registerDescription);
        AccountID registerAccountID = AccountID.createAccountID(registerDenomination, studentsID);

        //General Account

        String generalDenomination = "General";
        String generalDescription = "My general";

        us002_1CreateGroupService.addAccountToGroup(studentsID, generalDenomination, generalDescription);
        AccountID generalAccountID = AccountID.createAccountID(generalDenomination, studentsID);

        //Categories Students
        //Groceries

        String groceriesDenomination = "Groceries";

        us002_1CreateGroupService.addCategoryToGroup(studentsID, groceriesDenomination);
        CategoryID groceriesCategoryID = CategoryID.createCategoryID(groceriesDenomination, studentsID);

        //Categories Students
        //Tuition

        String tuitionDenomination = "Tuition";

        us002_1CreateGroupService.addCategoryToGroup(studentsID, tuitionDenomination);
        CategoryID tuitionCategoryID = CategoryID.createCategoryID(tuitionDenomination, studentsID);

        //Transactions Students

        //Vegetables

        String vegetablesDebit = "debit";
        String vegetablesDescription = "Vegetables";
        double vegetablesAmount = 30;
        LocalDate vegetablesDate = LocalDate.now();
        us002_1CreateGroupService.addGroupTransaction(studentsID, groceriesCategoryID, vegetablesDebit, vegetablesDescription, vegetablesAmount, vegetablesDate, walletAccountID, generalAccountID);

        //Tuition ISEP

        String tuitionDebit = "debit";
        String tuitionDescription = "Tuition ISEP";
        double tuitionAmount = 1000;
        LocalDate tuitionDate = LocalDate.of(2020, 07, 02);
        us002_1CreateGroupService.addGroupTransaction(studentsID, tuitionCategoryID, tuitionDebit, tuitionDescription, tuitionAmount, tuitionDate, walletAccountID, generalAccountID);

        //Vege

        String vegeDebit = "debit";
        String vegeDescription = "Vegetables";
        double vegeAmount = 75;
        LocalDate vegeDate = LocalDate.of(2019, 05, 15);
        us002_1CreateGroupService.addGroupTransaction(studentsID, groceriesCategoryID, vegeDebit, vegeDescription, vegeAmount, vegeDate, generalAccountID, registerAccountID);


        //Toilet paper for COVID

        String toiletDebit = "debit";
        String toiletDescription = "Toilet paper for COVID";
        double toiletAmount = 75;
        LocalDate toiletDate = LocalDate.of(2019, 05, 15);
        us002_1CreateGroupService.addGroupTransaction(studentsID, groceriesCategoryID, toiletDebit, toiletDescription, toiletAmount, toiletDate, walletAccountID, registerAccountID);

    }


    //--------------------Accounts--------------------//


    private static void createPersonAccounts(PersonID personID, CreatePersonService createPersonService) {

        //Company

        String companyDenomination = "Company";
        String companyDescription = "Company account";

//        Create CreatePersonAccountDTO
        CreatePersonAccountDTO createPersonCompanyAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(personID.getEmail().getEmail(), companyDescription, companyDenomination);

        createPersonService.addAccountToPerson(createPersonCompanyAccountDTO);


        //Bank Account

        String bankAccountDenomination = "Bank Account";
        String bankAccountDescription = "Personal bank account";

//        Create CreatePersonAccountDTO
        CreatePersonAccountDTO createPersonBankAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(personID.getEmail().getEmail(), bankAccountDescription, bankAccountDenomination);

        createPersonService.addAccountToPerson(createPersonBankAccountDTO);

        //Wallet

        String walletDenomination = "Wallet";
        String walletDescription = "My wallet";

//        Create CreatePersonAccountDTO
        CreatePersonAccountDTO createPersonWalletAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(personID.getEmail().getEmail(), walletDescription, walletDenomination);

        createPersonService.addAccountToPerson(createPersonWalletAccountDTO);

        //State

        String stateDenomination = "State";
        String stateDescription = "Payments to the state";

//        Create CreatePersonAccountDTO
        CreatePersonAccountDTO createPersonStateAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(personID.getEmail().getEmail(), stateDescription, stateDenomination);

        createPersonService.addAccountToPerson(createPersonStateAccountDTO);

        //Supermarket

        String supermarketDenomination = "Supermarket";
        String supermarketDescription = "Grocery shopping";

//        Create CreatePersonAccountDTO
        CreatePersonAccountDTO createPersonSuperMarketAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(personID.getEmail().getEmail(), supermarketDescription, supermarketDenomination);

        createPersonService.addAccountToPerson(createPersonSuperMarketAccountDTO);

        //Household Expenses

        String householdDenomination = "Household Expenses";
        String householdDescription = "Money spent on household expenses";

//        Create CreatePersonAccountDTO
        CreatePersonAccountDTO createPersonHouseAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(personID.getEmail().getEmail(), householdDescription, householdDenomination);

        createPersonService.addAccountToPerson(createPersonHouseAccountDTO);
        //Streaming Services

        String streamingDenomination = "Streaming Services";
        String streamingDescription = "Money spent on streaming services";

//        Create CreatePersonAccountDTO
        CreatePersonAccountDTO createPersonStreamingAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(personID.getEmail().getEmail(), streamingDescription, streamingDenomination);

        createPersonService.addAccountToPerson(createPersonStreamingAccountDTO);

    }


    //--------------------Categories--------------------//


    private static void createPersonCategories(PersonID personID, CreatePersonService createPersonService) {

        //Salary

        String salaryDenomination = "Salary";

//        Create AddSalaryCategoryDTO
        CreatePersonCategoryDTO addSalaryCategoryDTO = CreatePersonCategoryDTOAssembler.createDTOFromPrimitiveTypes(personID.getEmail().getEmail(), salaryDenomination);

        createPersonService.addCategoryToPerson(addSalaryCategoryDTO);

        //Draw Money

        String drawMoneyDenomination = "Draw Money";

//        Create AddDrawMoneyCategoryDTO
        CreatePersonCategoryDTO addDrawMoneyCategoryDTO = CreatePersonCategoryDTOAssembler.createDTOFromPrimitiveTypes(personID.getEmail().getEmail(), drawMoneyDenomination);

        createPersonService.addCategoryToPerson(addDrawMoneyCategoryDTO);

        //IRS

        String irsDenomination = "IRS";

//        Create AddIRSCategoryDTO
        CreatePersonCategoryDTO addIRSCategoryDTO = CreatePersonCategoryDTOAssembler.createDTOFromPrimitiveTypes(personID.getEmail().getEmail(), irsDenomination);

        createPersonService.addCategoryToPerson(addIRSCategoryDTO);

        //Food

        String foodDenomination = "Food";

//        Create AddIRSCategoryDTO
        CreatePersonCategoryDTO addFoodCategoryDTO = CreatePersonCategoryDTOAssembler.createDTOFromPrimitiveTypes(personID.getEmail().getEmail(), foodDenomination);

        createPersonService.addCategoryToPerson(addFoodCategoryDTO);

        //Water Bill

        String waterBillDenomination = "Water Bill";

//        Create AddIRSCategoryDTO
        CreatePersonCategoryDTO addWaterCategoryDTO = CreatePersonCategoryDTOAssembler.createDTOFromPrimitiveTypes(personID.getEmail().getEmail(), waterBillDenomination);

        createPersonService.addCategoryToPerson(addWaterCategoryDTO);

        //Netflix

        String netflixDenomination = "Netflix";

//        Create AddIRSCategoryDTO
        CreatePersonCategoryDTO addNetflixCategoryDTO = CreatePersonCategoryDTOAssembler.createDTOFromPrimitiveTypes(personID.getEmail().getEmail(), netflixDenomination);

        createPersonService.addCategoryToPerson(addNetflixCategoryDTO);

    }


    //--------------------Transactiobs--------------------//


    private static void createPersonTransactions(PersonID personID, CreatePersonService createPersonService) {

//        Get Email from Person ID
        String personID_email = personID.getEmail().getEmail();

        //AccountID

        AccountID companyAccountID = AccountID.createAccountID("Company", personID);
        AccountID bankAccountID = AccountID.createAccountID("Bank Account", personID);
        AccountID walletAccountID = AccountID.createAccountID("Wallet", personID);
        AccountID stateAccountID = AccountID.createAccountID("State", personID);
        AccountID supermarketAccountID = AccountID.createAccountID("Supermarket", personID);
        AccountID householdAccountID = AccountID.createAccountID("Household Expenses", personID);
        AccountID streamingsAccountID = AccountID.createAccountID("Streaming Services", personID);

//        Get Account from AccountID
        String companyAccountID_Account = companyAccountID.getDenomination().getDenomination();
        String bankAccountID_Account = bankAccountID.getDenomination().getDenomination();
        String walletAccountID_Account = walletAccountID.getDenomination().getDenomination();
        String stateAccountID_Account = stateAccountID.getDenomination().getDenomination();
        String supermarketAccountID_Account = supermarketAccountID.getDenomination().getDenomination();
        String householdAccountID_Account = householdAccountID.getDenomination().getDenomination();
        String streamingsAccountID_Account = streamingsAccountID.getDenomination().getDenomination();


        //CategoryID

        CategoryID salaryCategoryID = CategoryID.createCategoryID("Salary", personID);
        CategoryID drawCategoryID = CategoryID.createCategoryID("Draw Money", personID);
        CategoryID irsCategoryID = CategoryID.createCategoryID("IRS", personID);
        CategoryID foodCategoryID = CategoryID.createCategoryID("Food", personID);
        CategoryID waterCategoryID = CategoryID.createCategoryID("Water Bill", personID);
        CategoryID netflixCategoryID = CategoryID.createCategoryID("Netflix", personID);

//        Get category from CategoryID
        String salaryCategoryID_Category = salaryCategoryID.getDenomination().getDenomination();
        String drawCategoryID_Category = drawCategoryID.getDenomination().getDenomination();
        String irsCategoryID_Category = irsCategoryID.getDenomination().getDenomination();
        String foodCategoryID_Category = foodCategoryID.getDenomination().getDenomination();
        String waterCategoryID_Category = waterCategoryID.getDenomination().getDenomination();
        String netflixCategoryID_Category = netflixCategoryID.getDenomination().getDenomination();

        //Types

        String credit = "credit";
        String debit = "debit";

        //Salary January

        String salaryJanuaryDescription = "January salary";
        double salaryJanuaryAmount = 1500;
        String salaryJanuaryDate = "2020-01-21";
//        Create a Person Transaction DTO
        CreatePersonTransactionDTO createPersonJanuarySalaryTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(personID_email,
                salaryCategoryID_Category, credit, salaryJanuaryDescription, salaryJanuaryAmount, companyAccountID_Account, bankAccountID_Account, salaryJanuaryDate);

        createPersonService.addPersonTransaction(createPersonJanuarySalaryTransactionDTO);

        createPersonService.addPersonTransaction(createPersonJanuarySalaryTransactionDTO);

        //Draw Money January

        String drawJanuaryDescription = "January draw money";
        double drawJanuaryAmount = 100;
        String drawJanuaryDate = "2020-01-25";
//        Create a Person Transaction DTO
        CreatePersonTransactionDTO createPersonDrawMoneyTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(personID_email,
                drawCategoryID_Category, credit, drawJanuaryDescription, drawJanuaryAmount, bankAccountID_Account, walletAccountID_Account, drawJanuaryDate);

        createPersonService.addPersonTransaction(createPersonDrawMoneyTransactionDTO);

        //IRS January

        String irsJanuaryDescription = "January IRS";
        double irsJanuaryAmount = 150;
        String irsJanuaryDate = "2020-01-27";
//        Create a Person Transaction DTO
        CreatePersonTransactionDTO createPersonIRSTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(personID_email,
                irsCategoryID_Category, debit, irsJanuaryDescription, irsJanuaryAmount, bankAccountID_Account, stateAccountID_Account, irsJanuaryDate);

        createPersonService.addPersonTransaction(createPersonIRSTransactionDTO);

        //Food January

        String foodJanuaryDescription = "January food";
        double foodJanuaryAmount = 50;
        String foodJanuaryDate = "2020-01-29";
//        Create a Person Transaction DTO
        CreatePersonTransactionDTO createPersonFoodTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(personID_email,
                foodCategoryID_Category, debit, foodJanuaryDescription, foodJanuaryAmount, walletAccountID_Account, supermarketAccountID_Account, foodJanuaryDate);

        createPersonService.addPersonTransaction(createPersonFoodTransactionDTO);

        //Water Bill January

        String waterJanuaryDescription = "January water bill";
        double waterJanuaryAmount = 50;
        String waterJanuaryDate = "2020-01-30";
//        Create a Person Transaction DTO
        CreatePersonTransactionDTO createPersonWaterBillTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(personID_email,
                waterCategoryID_Category, debit, waterJanuaryDescription, waterJanuaryAmount, bankAccountID_Account, householdAccountID_Account, waterJanuaryDate);

        createPersonService.addPersonTransaction(createPersonWaterBillTransactionDTO);

        //Streaming Services January

        String streamingJanuaryDescription = "January netflix";
        double streamingJanuaryAmount = 25;
        String streamingJanuaryDate = "2020-01-30";
//        Create a Person Transaction DTO
        CreatePersonTransactionDTO createPersonStramingServicesDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(personID_email,
                netflixCategoryID_Category, debit, streamingJanuaryDescription, streamingJanuaryAmount, bankAccountID_Account, streamingsAccountID_Account, streamingJanuaryDate);

        createPersonService.addPersonTransaction(createPersonStramingServicesDTO);

        //Salary February

        String salaryFebruaryDescription = "February salary";
        double salaryFebruaryAmount = 1500;
        String salaryFebruaryDate = "2020-02-21";
//        Create a Person Transaction DTO
        CreatePersonTransactionDTO createPersonFebruarySalaryTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(personID_email,
                salaryCategoryID_Category, credit, salaryFebruaryDescription, salaryFebruaryAmount, companyAccountID_Account, bankAccountID_Account, salaryFebruaryDate);

        createPersonService.addPersonTransaction(createPersonFebruarySalaryTransactionDTO);

    }

    //--------------------Group Accounts--------------------//


    private static void createGroupAccounts(GroupID groupID, US002_1CreateGroupService us002_1CreateGroupService) {

        //Company

        String companyDenomination = "Company";
        String companyDescription = "Company account";

        us002_1CreateGroupService.addAccountToGroup(groupID, companyDenomination, companyDescription);

        //Bank Account

        String bankAccountDenomination = "Bank Account";
        String bankAccountDescription = "Personal bank account";

        us002_1CreateGroupService.addAccountToGroup(groupID, bankAccountDenomination, bankAccountDescription);

        //Wallet

        String walletDenomination = "Wallet";
        String walletDescription = "My wallet";

        us002_1CreateGroupService.addAccountToGroup(groupID, walletDenomination, walletDescription);

        //State

        String stateDenomination = "State";
        String stateDescription = "Payments to the state";

        us002_1CreateGroupService.addAccountToGroup(groupID, stateDenomination, stateDescription);

        //Supermarket

        String supermarketDenomination = "Supermarket";
        String supermarketDescription = "Grocery shopping";

        us002_1CreateGroupService.addAccountToGroup(groupID, supermarketDenomination, supermarketDescription);

        //Household Expenses

        String householdDenomination = "Household Expenses";
        String householdDescription = "Money spent on household expenses";

        us002_1CreateGroupService.addAccountToGroup(groupID, householdDenomination, householdDescription);

        //Streaming Services

        String streamingDenomination = "Streaming Services";
        String streamingDescription = "Money spent on streaming services";

        us002_1CreateGroupService.addAccountToGroup(groupID, streamingDenomination, streamingDescription);

    }


    //-------------------- Group Categories--------------------//


    private static void createGroupCategories(GroupID groupID, US002_1CreateGroupService us002_1CreateGroupService) {

        //Salary

        String salaryDenomination = "Salary";

        us002_1CreateGroupService.addCategoryToGroup(groupID, salaryDenomination);

        //Draw Money

        String drawMoneyDenomination = "Draw Money";

        us002_1CreateGroupService.addCategoryToGroup(groupID, drawMoneyDenomination);

        //IRS

        String irsDenomination = "IRS";

        us002_1CreateGroupService.addCategoryToGroup(groupID, irsDenomination);

        //Food

        String foodDenomination = "Food";

        us002_1CreateGroupService.addCategoryToGroup(groupID, foodDenomination);

        //Water Bill

        String waterBillDenomination = "Water Bill";

        us002_1CreateGroupService.addCategoryToGroup(groupID, waterBillDenomination);

        //Netflix

        String netflixDenomination = "Netflix";

        us002_1CreateGroupService.addCategoryToGroup(groupID, netflixDenomination);

    }


    //--------------------Group Transactiobs--------------------//


    private static void createGroupTransactions(GroupID groupID, US002_1CreateGroupService us002_1CreateGroupService) {

        //AccountID

        AccountID companyAccountID = AccountID.createAccountID("Company", groupID);
        AccountID bankAccountID = AccountID.createAccountID("Bank Account", groupID);
        AccountID walletAccountID = AccountID.createAccountID("Wallet", groupID);
        AccountID stateAccountID = AccountID.createAccountID("State", groupID);
        AccountID supermarketAccountID = AccountID.createAccountID("Supermarket", groupID);
        AccountID householdAccountID = AccountID.createAccountID("Household Expenses", groupID);
        AccountID streamingsAccountID = AccountID.createAccountID("Streaming Services", groupID);

        //CategoryID

        CategoryID salaryCategoryID = CategoryID.createCategoryID("Salary", groupID);
        CategoryID drawCategoryID = CategoryID.createCategoryID("Draw Money", groupID);
        CategoryID irsCategoryID = CategoryID.createCategoryID("IRS", groupID);
        CategoryID foodCategoryID = CategoryID.createCategoryID("Food", groupID);
        CategoryID waterCategoryID = CategoryID.createCategoryID("Water Bill", groupID);
        CategoryID netflixCategoryID = CategoryID.createCategoryID("Netflix", groupID);

        //Types

        String credit = "credit";
        String debit = "debit";

        //Salary January

        String salaryJanuaryDescription = "January salary";
        double salaryJanuaryAmount = 1500;
        LocalDate salaryJanuaryDate = LocalDate.of(2020, 01, 21);
        us002_1CreateGroupService.addGroupTransaction(groupID, salaryCategoryID, credit, salaryJanuaryDescription, salaryJanuaryAmount, salaryJanuaryDate, companyAccountID, bankAccountID);

        //Draw Money January

        String drawJanuaryDescription = "January draw money";
        double drawJanuaryAmount = 100;
        LocalDate drawJanuaryDate = LocalDate.of(2020, 01, 25);
        us002_1CreateGroupService.addGroupTransaction(groupID, drawCategoryID, credit, drawJanuaryDescription, drawJanuaryAmount, drawJanuaryDate, bankAccountID, walletAccountID);

        //IRS January

        String irsJanuaryDescription = "January IRS";
        double irsJanuaryAmount = 150;
        LocalDate irsJanuaryDate = LocalDate.of(2020, 01, 27);
        us002_1CreateGroupService.addGroupTransaction(groupID, irsCategoryID, debit, irsJanuaryDescription, irsJanuaryAmount, irsJanuaryDate, bankAccountID, stateAccountID);

        //Food January

        String foodJanuaryDescription = "January food";
        double foodJanuaryAmount = 50;
        LocalDate foodJanuaryDate = LocalDate.of(2020, 01, 29);
        us002_1CreateGroupService.addGroupTransaction(groupID, foodCategoryID, debit, foodJanuaryDescription, foodJanuaryAmount, foodJanuaryDate, walletAccountID, supermarketAccountID);

        //Water Bill January

        String waterJanuaryDescription = "January water bill";
        double waterJanuaryAmount = 50;
        LocalDate waterJanuaryDate = LocalDate.of(2020, 01, 30);
        us002_1CreateGroupService.addGroupTransaction(groupID, waterCategoryID, debit, waterJanuaryDescription, waterJanuaryAmount, waterJanuaryDate, bankAccountID, householdAccountID);

        //Streaming Services January

        String streaingJanuaryDescription = "January netflix";
        double streamingJanuaryAmount = 25;
        LocalDate streamingJanuaryDate = LocalDate.of(2020, 01, 30);
        us002_1CreateGroupService.addGroupTransaction(groupID, netflixCategoryID, debit, streaingJanuaryDescription, streamingJanuaryAmount, streamingJanuaryDate, bankAccountID, streamingsAccountID);

        //Salary February

        String salaryFebruaryDescription = "February salary";
        double salaryFebruaryAmount = 1500;
        LocalDate salaryFebruaryDate = LocalDate.of(2020, 02, 21);
        us002_1CreateGroupService.addGroupTransaction(groupID, salaryCategoryID, credit, salaryFebruaryDescription, salaryFebruaryAmount, salaryFebruaryDate, companyAccountID, bankAccountID);

    }
}
