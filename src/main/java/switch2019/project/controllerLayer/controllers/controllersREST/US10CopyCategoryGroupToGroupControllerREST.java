package switch2019.project.controllerLayer.controllers.controllersREST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import switch2019.project.applicationLayer.applicationServices.US10CopyCategoryGroupToGroupService;
import switch2019.project.dtoLayer.dtos.CopyCategoryGroupToGroupDTO;
import switch2019.project.dtoLayer.dtos.GroupDTO;
import switch2019.project.dtoLayer.dtos.NewGroupCategoryInfoDTO;
import switch2019.project.dtoLayer.dtosAssemblers.CopyCategoryGroupToGroupDTOAssembler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class US10CopyCategoryGroupToGroupControllerREST {

    @Autowired
    private US10CopyCategoryGroupToGroupService service;

    @PostMapping("/persons/{personEmail}/groups/{groupDenominationFrom}/categories/groups/{groupDenominationTo}")
    public ResponseEntity<Object> copyGroupToGroupCategory(@RequestBody NewGroupCategoryInfoDTO info,
                                                           @PathVariable final String personEmail,
                                                           @PathVariable final String groupDenominationFrom,
                                                           @PathVariable final String groupDenominationTo) {

        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO = CopyCategoryGroupToGroupDTOAssembler.createDTOFromPrimitiveTypes(personEmail, groupDenominationFrom, groupDenominationTo, info.getCategoryDenomination());

        GroupDTO result = service.copyCategoryAsAdmin(copyCategoryGroupToGroupDTO);

        Link link_to_admins = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupAdmins(groupDenominationTo)).withRel("admins");
        Link link_to_members = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupMembers(groupDenominationTo)).withRel("members");
        Link link_to_ledger = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupLedger(groupDenominationTo)).withRel("ledger");
        Link link_to_accounts = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupAccounts(personEmail, groupDenominationTo)).withRel("accounts");
        Link link_to_categories = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupCategories(personEmail, groupDenominationTo)).withRel("categories");

        result.add(link_to_admins);
        result.add(link_to_members);
        result.add(link_to_ledger);
        result.add(link_to_accounts);
        result.add(link_to_categories);

        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

}
