package switch2019.project.controllerLayer.controllers.controllersREST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switch2019.project.applicationLayer.applicationServices.US008_1CreateGroupTransactionService;
import switch2019.project.dtoLayer.dtos.*;
import switch2019.project.dtoLayer.dtosAssemblers.CreateGroupTransactionDTOAssembler;
import switch2019.project.dtoLayer.dtosAssemblers.DeleteGroupTransactionDTOAssembler;
import switch2019.project.dtoLayer.dtosAssemblers.UpdateGroupTransactionDTOAssembler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class US008_1CreateGroupTransactionControllerREST {

    @Autowired
    private US008_1CreateGroupTransactionService service;

    //US008.1 Como membro de grupo, quero criar um movimento , atribuindo-lhe um valor, a data (atual, por omissão), uma descrição, uma categoria, uma conta de crédito e outra de débito.

    @PostMapping("/persons/{personEmail}/groups/{groupDenomination}/ledgers/records")
    public ResponseEntity<Object> createGroupTransaction(@RequestBody NewGroupTransactionInfoDTO info,
                                                         @PathVariable final String personEmail,
                                                         @PathVariable final String groupDenomination) {

        CreateGroupTransactionDTO createGroupTransactionDTO = CreateGroupTransactionDTOAssembler.createDTOFromPrimitiveTypes(groupDenomination, personEmail, info.getDenominationCategory(), info.getDenominationAccountDeb(), info.getDenominationAccountCred(), info.getAmount(), info.getType(), info.getDescription(), info.getDate());

        GroupDTO result = service.createGroupTransaction(createGroupTransactionDTO);

        Link link_to_admins = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupAdmins(groupDenomination)).withRel("admins");
        Link link_to_members = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupMembers(groupDenomination)).withRel("members");
        Link link_to_ledger = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupLedger(groupDenomination)).withRel("records");
        Link link_to_accounts = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupAccounts(personEmail, groupDenomination)).withRel("accounts");
        Link link_to_categories = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupCategories(personEmail, groupDenomination)).withRel("categories");

        result.add(link_to_admins);
        result.add(link_to_members);
        result.add(link_to_ledger);
        result.add(link_to_accounts);
        result.add(link_to_categories);

        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @PutMapping("/persons/{personEmail}/groups/{groupDenomination}/ledgers/records/{transactionNumber}")
    public ResponseEntity<Object> updateGroupTransaction(@RequestBody NewGroupTransactionInfoDTO info,
                                                         @PathVariable final String personEmail,
                                                         @PathVariable final String groupDenomination,
                                                         @PathVariable final int transactionNumber) {

        UpdateGroupTransactionDTO updateGroupTransactionDTO = UpdateGroupTransactionDTOAssembler.createDTOFromPrimitiveTypes(transactionNumber, groupDenomination, personEmail, info.getDenominationCategory(), info.getDenominationAccountDeb(), info.getDenominationAccountCred(), info.getAmount(), info.getType(), info.getDescription());

        GroupDTO result = service.updateGroupTransaction(updateGroupTransactionDTO);

        Link link_to_admins = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupAdmins(groupDenomination)).withRel("admins");
        Link link_to_members = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupMembers(groupDenomination)).withRel("members");
        Link link_to_ledger = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupLedger(groupDenomination)).withRel("records");
        Link link_to_accounts = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupAccounts(personEmail, groupDenomination)).withRel("accounts");
        Link link_to_categories = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupCategories(personEmail, groupDenomination)).withRel("categories");

        result.add(link_to_admins);
        result.add(link_to_members);
        result.add(link_to_ledger);
        result.add(link_to_accounts);
        result.add(link_to_categories);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @DeleteMapping("/persons/{personEmail}/groups/{groupDenomination}/ledgers/records/{transactionNumber}")
    public ResponseEntity<Object> deleteGroupTransaction(@PathVariable final String personEmail,
                                                         @PathVariable final String groupDenomination,
                                                         @PathVariable final int transactionNumber) {

        DeleteGroupTransactionDTO deleteGroupTransactionDTO = DeleteGroupTransactionDTOAssembler.createDTOFromPrimitiveTypes(transactionNumber, groupDenomination, personEmail);

        GroupDTO result = service.deleteGroupTransaction(deleteGroupTransactionDTO);

        Link link_to_admins = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupAdmins(groupDenomination)).withRel("admins");
        Link link_to_members = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupMembers(groupDenomination)).withRel("members");
        Link link_to_ledger = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupLedger(groupDenomination)).withRel("records");
        Link link_to_accounts = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupAccounts(personEmail, groupDenomination)).withRel("accounts");
        Link link_to_categories = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupCategories(personEmail, groupDenomination)).withRel("categories");

        result.add(link_to_admins);
        result.add(link_to_members);
        result.add(link_to_ledger);
        result.add(link_to_accounts);
        result.add(link_to_categories);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
