package switch2019.project.applicationLayer.applicationServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import switch2019.project.dtoLayer.dtos.GroupDTO;
import switch2019.project.dtoLayer.dtos.GroupIDDTO;
import switch2019.project.dtoLayer.dtos.GroupMembersDTO;
import switch2019.project.dtoLayer.dtos.GroupsThatAreFamilyDTO;
import switch2019.project.dtoLayer.dtosAssemblers.GroupDTOAssembler;
import switch2019.project.dtoLayer.dtosAssemblers.GroupIDDTOAssembler;
import switch2019.project.dtoLayer.dtosAssemblers.GroupMembersDTOAssembler;
import switch2019.project.dtoLayer.dtosAssemblers.GroupsThatAreFamilyDTOAssembler;
import switch2019.project.domainLayer.domainEntities.aggregates.group.Group;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Person;
import switch2019.project.domainLayer.domainEntities.vosShared.*;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;
import switch2019.project.domainLayer.repositoriesInterfaces.IGroupRepository;
import switch2019.project.domainLayer.repositoriesInterfaces.IPersonRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * The type Us 004 groups that are family service.
 *
 * @author Ala Matos
 */
/*
 * Ala Matos created on 19/03/2020
 * inside the PACKAGE switch2019.project.Services
 */

@Service
public class US004GroupsThatAreFamilyService {

    @Autowired
    private final IGroupRepository groupRepository;

    @Autowired
    private final IPersonRepository personRepository;

    /**
     * Instantiates a new Us 004 groups that are family service.
     *
     * @param groupRepository  the group repository
     * @param personRepository the person repository
     */

    /**
     * The constant GROUP_DOES_NOT_EXIST.
     */
    public static final String GROUP_DOES_NOT_EXIST = "Group does not exist";


    public US004GroupsThatAreFamilyService(IGroupRepository groupRepository, IPersonRepository personRepository) {
        this.groupRepository = groupRepository;
        this.personRepository = personRepository;
    }

    /**
     * Groups that are family groups that are family dto.
     *
     * @return the groups that are family dto
     */
    public GroupsThatAreFamilyDTO groupsThatAreFamily() {
        List<Group> listGroups = this.groupRepository.findAll();
        List<GroupIDDTO> listToReturn = new ArrayList<>();
        GroupID groupID;
        for (Group group : listGroups) {
            groupID = isFamily(group);
            if (groupID != null) {
                listToReturn.add(GroupIDDTOAssembler.createDTOFromDomainObject(groupID));
            }
        }
        return GroupsThatAreFamilyDTOAssembler.createDTOFromDomainObject(listToReturn);
    }

    private GroupID isFamily(Group group) {
        List<PersonID> allMembers = group.getAllMembers();
        List<PersonID> allMembersAux = new ArrayList<>();
        boolean father = false;
        boolean mother = false;
        for (int i = 0; i < allMembers.size(); i++) {
            for (int j = 0; j < allMembers.size(); j++) {
                Optional<Person> opPersonA = this.personRepository.findById(allMembers.get(j));
                if (opPersonA.isPresent()) {
                    Person personA = opPersonA.get();
                    boolean isFather = allMembers.get(i).equals(personA.getFather());
                    boolean isMother = allMembers.get(i).equals(personA.getMother());
                    if ((i != j) && isFather || isMother) {
                        //Find father
                        if (!allMembersAux.contains(allMembers.get(i)) && isFather && !father) {
                            allMembersAux.add(allMembers.get(i));
                            father = true;
                        }
                        //Find mother
                        if (!allMembersAux.contains(allMembers.get(i)) && isMother && !mother) {
                            allMembersAux.add(allMembers.get(i));
                            mother = true;
                        }
                        //Add the children in case of find a father or a mother
                        if (!allMembersAux.contains(allMembers.get(j))) {
                            allMembersAux.add(allMembers.get(j));
                        }
                    }
                }
            }
        }
        //Return null if father or mother is null
        if (!mother || !father) {
            return null;
        }
        //Compare the List's
        for (PersonID personID : allMembers) {
            if (!allMembersAux.contains(personID)) {
                return null;
            }
        }
        return group.getGroupID();
    }

    @Transactional
    public GroupDTO getGroupByDenomination(String denomination) {
        Group group;
        GroupID groupID = GroupID.createGroupID(denomination);
        Optional<Group> opGroup = groupRepository.findById(groupID);
        if (!opGroup.isPresent() ) {

            throw new NotFoundArgumentsBusinessException(GROUP_DOES_NOT_EXIST);

        } else {
            group = opGroup.get();
        }

        return GroupDTOAssembler.createDTOFromDomainObject(group.getGroupID().getDenomination(), group.getDescription(), group.getDateOfCreation());

    }

    @Transactional
    public GroupMembersDTO getGroupAllMembers(String denomination) {
        Group group;
        GroupID groupID = GroupID.createGroupID(denomination);
        Optional<Group> opGroup = groupRepository.findById(groupID);
        if (!opGroup.isPresent() ) {

            throw new NotFoundArgumentsBusinessException(GROUP_DOES_NOT_EXIST);

        } else {
            group = opGroup.get();

        }
        List<PersonID> allMembers = group.getAllMembers();


        return GroupMembersDTOAssembler.createDTOFromDomainObject(allMembers);
    }
}