package switch2019.project.applicationLayer.applicationServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switch2019.project.domainLayer.domainEntities.aggregates.group.Group;
import switch2019.project.domainLayer.domainEntities.vosShared.CategoryID;
import switch2019.project.domainLayer.domainEntities.vosShared.GroupID;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;
import switch2019.project.domainLayer.repositoriesInterfaces.ICategoryRepository;
import switch2019.project.domainLayer.repositoriesInterfaces.IGroupRepository;
import switch2019.project.dtoLayer.dtos.CopyCategoryGroupToGroupDTO;
import switch2019.project.dtoLayer.dtos.GroupDTO;
import switch2019.project.dtoLayer.dtosAssemblers.GroupDTOAssembler;

import java.util.Optional;

@Service
public class US10CopyCategoryGroupToGroupService {

    @Autowired
    private IGroupRepository groupRepository;
    @Autowired
    private ICategoryRepository categoryRepository;

    // Return message

    public final static String SUCCESS = "Category copied and added";

    public final static String PERSON_NOT_IN_CHARGE = "Person is not in charge";

    public final static String CATEGORY_DOES_NOT_EXIST = "Category does not exist";

    public final static String GROUP_DOES_NOT_EXIST = "Group does not exist";

    public final static String CATEGORY_ALREADY_EXIST_IN_GROUP = "Category already exists in the group";


    // Constructor

    public US10CopyCategoryGroupToGroupService(IGroupRepository groupRepository, ICategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
        this.groupRepository = groupRepository;
    }

    // Copy Category

    public GroupDTO copyCategoryAsAdmin(CopyCategoryGroupToGroupDTO copyCategoryGroupFromGroupDTO) {

        Group groupTo;
        Group groupFrom;

        GroupID groupToID = GroupID.createGroupID(copyCategoryGroupFromGroupDTO.getGroupDenominationTo());
        Optional<Group> opGroupTo = groupRepository.findById(groupToID);

        GroupID groupFromID = GroupID.createGroupID(copyCategoryGroupFromGroupDTO.getGroupDenominationFrom());
        Optional<Group> opGroupFrom = groupRepository.findById(groupFromID);

        if (!opGroupTo.isPresent() || !opGroupFrom.isPresent()) {

            throw new NotFoundArgumentsBusinessException(GROUP_DOES_NOT_EXIST);

        } else {

            groupTo = opGroupTo.get();
            groupFrom = opGroupFrom.get();

            // If Person is part of the PeopleInCharge of a group, he/she already exists in personRepository

            PersonID personID = PersonID.createPersonID(copyCategoryGroupFromGroupDTO.getPersonEmail());
            boolean isPeopleInChargeGroupTo = groupTo.isPersonPeopleInCharge(personID);
            boolean isPeopleInChargeGroupFrom = groupFrom.isPersonPeopleInCharge(personID);

            CategoryID categoryIDGroupFrom = CategoryID.createCategoryID(copyCategoryGroupFromGroupDTO.getCategoryDenomination(), groupFromID);
            boolean categoryGroupFromExistsInRepo = categoryRepository.existsById(categoryIDGroupFrom);

            CategoryID categoryIDGroupTo = CategoryID.createCategoryID(copyCategoryGroupFromGroupDTO.getCategoryDenomination(), groupToID);
            boolean categoryGroupToExistsInRepo = categoryRepository.existsById(categoryIDGroupTo);

            if (!isPeopleInChargeGroupTo || !isPeopleInChargeGroupFrom) {
                throw new InvalidArgumentsBusinessException(PERSON_NOT_IN_CHARGE);
            }

            if (!categoryGroupFromExistsInRepo) {
                throw new InvalidArgumentsBusinessException(CATEGORY_DOES_NOT_EXIST);
            }

            if (categoryGroupToExistsInRepo) {
                throw new InvalidArgumentsBusinessException(CATEGORY_ALREADY_EXIST_IN_GROUP);

            } else {
                groupTo.addCategory(categoryIDGroupFrom);
                groupRepository.addAndSaveCategory(groupTo);
            }
        }
        GroupDTO groupDTO = GroupDTOAssembler.createDTOFromDomainObject(
                groupTo.getGroupID().getDenomination(),
                groupTo.getDescription(),
                groupTo.getDateOfCreation());

        return groupDTO;
    }

}
