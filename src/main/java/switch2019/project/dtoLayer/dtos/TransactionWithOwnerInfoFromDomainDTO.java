package switch2019.project.dtoLayer.dtos;

import java.util.Objects;

/**
 * @author Ala Matos
 */
public class TransactionWithOwnerInfoFromDomainDTO {
    private String ownerInfo;
    private String category;
    private String type;
    private String description;
    private double amount;
    private String date;
    private String debitAccount;
    private String creditAccount;

    /**
     * Instantiates a new TransactionWithOwnerInfoFromDomainDTO
     */
    public TransactionWithOwnerInfoFromDomainDTO() {
        this.ownerInfo = "";
        this.category = "";
        this.type = "";
        this.description = "";
        this.amount = 0;
        this.date = "";
        this.debitAccount = "";
        this.creditAccount = "";
    }

    /**
     * Instantiates a new TransactionWithOwnerInfoFromDomainDTO, with the following parameters
     *
     * @param ownerInfo
     * @param category
     * @param type
     * @param description
     * @param amount
     * @param date
     * @param debitAccount
     * @param creditAccount
     */
    public TransactionWithOwnerInfoFromDomainDTO(String ownerInfo, String category, String type, String description,
                                                 double amount, String date, String debitAccount, String creditAccount) {
        this.ownerInfo = ownerInfo;
        this.category = category;
        this.type = type;
        this.description = description;
        this.amount = amount;
        this.date = date;
        this.debitAccount = debitAccount;
        this.creditAccount = creditAccount;
    }

    /**
     * Method to get Owner Info
     *
     * @return String
     */
    public String getOwnerInfo() {
        return ownerInfo;
    }

    /**
     * Method to set Owner Info
     *
     * @param ownerInfo
     */
    public void setOwnerInfo(String ownerInfo) {
        this.ownerInfo = ownerInfo;
    }

    /**
     * Method to get Category
     *
     * @return String
     */
    public String getCategory() {
        return category;
    }

    /**
     * Method to set Category
     *
     * @param category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * Method to get Type
     *
     * @return String
     */
    public String getType() {
        return type;
    }

    /**
     * Method to set Type
     *
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Method to get Description
     *
     * @return String
     */
    public String getDescription() {
        return description;
    }

    /**
     * Method to set Description
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Method to get Amount
     *
     * @return double
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Method to set Amount
     *
     * @param amount
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * Method to get Date
     *
     * @return String
     */
    public String getDate() {
        return date;
    }

    /**
     * Method to set Date
     *
     * @param date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Method to get Debit Account
     *
     * @return String
     */
    public String getDebitAccount() {
        return debitAccount;
    }

    /**
     * Method to set Debit Account
     *
     * @param debitAccount
     */
    public void setDebitAccount(String debitAccount) {
        this.debitAccount = debitAccount;
    }

    /**
     * Method to get Credit Account
     *
     * @return String
     */
    public String getCreditAccount() {
        return creditAccount;
    }

    /**
     * Method to set Credit Account
     *
     * @param creditAccount
     */
    public void setCreditAccount(String creditAccount) {
        this.creditAccount = creditAccount;
    }

    /**
     * Equals method
     *
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionWithOwnerInfoFromDomainDTO that = (TransactionWithOwnerInfoFromDomainDTO) o;
        return Double.compare(that.amount, amount) == 0 &&
                Objects.equals(ownerInfo, that.ownerInfo) &&
                Objects.equals(category, that.category) &&
                Objects.equals(type, that.type) &&
                Objects.equals(description, that.description) &&
                Objects.equals(date, that.date) &&
                Objects.equals(debitAccount, that.debitAccount) &&
                Objects.equals(creditAccount, that.creditAccount);
    }

    /**
     * Hash code method
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(ownerInfo, category, type, description, amount, date, debitAccount, creditAccount);
    }
}
