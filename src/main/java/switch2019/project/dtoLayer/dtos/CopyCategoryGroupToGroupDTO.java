package switch2019.project.dtoLayer.dtos;

import java.util.Objects;

public class CopyCategoryGroupToGroupDTO {

    private final String personEmail;
    private final String groupDenominationFrom;  // group A
    private final String groupDenominationTo;    // group B
    private final String categoryDenomination;


    // Constructor

    public CopyCategoryGroupToGroupDTO(String personEmail, String groupDenominationFrom, String groupDenominationTo, String categoryDenomination){
        this.personEmail = personEmail;
        this.groupDenominationFrom = groupDenominationFrom;
        this.groupDenominationTo = groupDenominationTo;
        this.categoryDenomination = categoryDenomination;
    }


    // Getters

    public String getPersonEmail() {
        return personEmail;
    }

    public String getGroupDenominationFrom() {
        return groupDenominationFrom;
    }

    public String getGroupDenominationTo() {
        return groupDenominationTo;
    }

    public String getCategoryDenomination() {
        return categoryDenomination;
    }


    // Equals & hashCode

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CopyCategoryGroupToGroupDTO)) return false;
        CopyCategoryGroupToGroupDTO that = (CopyCategoryGroupToGroupDTO) o;
        return Objects.equals(getPersonEmail(), that.getPersonEmail()) &&
                Objects.equals(getGroupDenominationFrom(), that.getGroupDenominationFrom()) &&
                Objects.equals(getGroupDenominationTo(), that.getGroupDenominationTo()) &&
                Objects.equals(getCategoryDenomination(), that.getCategoryDenomination());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPersonEmail(), getGroupDenominationFrom(), getGroupDenominationTo(), getCategoryDenomination());
    }
}
