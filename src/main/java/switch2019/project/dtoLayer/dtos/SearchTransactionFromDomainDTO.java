package switch2019.project.dtoLayer.dtos;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Ala Matos
 */
public class SearchTransactionFromDomainDTO {
    private List<TransactionWithOwnerInfoFromDomainDTO> transactions;

    /**
     * Instantiates a new SearchTransactionFromDomainDTO
     */
    public SearchTransactionFromDomainDTO() {
        this.transactions = new ArrayList<>();
    }

    /**
     * Method to get TransactionWithOwnerInfoFromDomainDTO list
     *
     * @return List<TransactionWithOwnerInfoFromDomainDTO>
     */
    public List<TransactionWithOwnerInfoFromDomainDTO> getTransactions() {
        return transactions;
    }

    /**
     * Method to set TransactionWithOwnerInfoFromDomainDTO list
     *
     * @param transactions
     */
    public void setTransactions(List<TransactionWithOwnerInfoFromDomainDTO> transactions) {
        this.transactions = transactions;
    }

    /**
     * Equals method
     *
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SearchTransactionFromDomainDTO that = (SearchTransactionFromDomainDTO) o;
        return Objects.equals(transactions, that.transactions);
    }

    /**
     * Hash code method
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(transactions);
    }
}
