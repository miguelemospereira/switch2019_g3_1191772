package switch2019.project.dtoLayer.dtos;


import java.util.Objects;

/**
 * @author Ala Matos
 */
public class SearchTransactionDTO {
    private String personID;
    private String startDate;
    private String endDate;

    /**
     * Instantiates a new SearchTransactionDTO
     *
     * @param personID
     * @param startDate
     * @param endDate
     */
    public SearchTransactionDTO(String personID, String startDate, String endDate) {
        this.personID = personID;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    /**
     * Method to get start date
     *
     * @return String
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Method to get end date
     *
     * @return String
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * Method to get PersonID
     *
     * @return String
     */
    public String getPersonID() {
        return personID;
    }

    /**
     * Equals method
     *
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SearchTransactionDTO that = (SearchTransactionDTO) o;
        return Objects.equals(personID, that.personID) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate);
    }

    /**
     * Hash code method
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(personID, startDate, endDate);
    }
}
