package switch2019.project.dtoLayer.dtosAssemblers;

import switch2019.project.dtoLayer.dtos.SearchTransactionDTO;

/**
 * @author Ala Matos
 */
public class SearchTransactionDTOAssembler {
    public static SearchTransactionDTO createDTOFromPrimitiveType (String personID, String startDate, String endDate) {

        return new SearchTransactionDTO(personID, startDate, endDate);
    }

}
