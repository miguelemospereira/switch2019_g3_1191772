package switch2019.project.dtoLayer.dtosAssemblers;

import switch2019.project.dtoLayer.dtos.CopyCategoryGroupToGroupDTO;

public class CopyCategoryGroupToGroupDTOAssembler {

    public static CopyCategoryGroupToGroupDTO createDTOFromPrimitiveTypes
            (String personEmail, String groupDenominationFrom, String groupDenominationTo, String categoryDenomination) {

        CopyCategoryGroupToGroupDTO copyCategoryGroupToGroupDTO = new CopyCategoryGroupToGroupDTO(
                personEmail, groupDenominationFrom, groupDenominationTo, categoryDenomination);

        return copyCategoryGroupToGroupDTO;
    }
}
