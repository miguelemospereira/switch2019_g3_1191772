package switch2019.project.dtoLayer.dtosAssemblers;


import switch2019.project.domainLayer.domainEntities.aggregates.ledger.Transaction;
import switch2019.project.dtoLayer.dtos.TransactionWithOwnerInfoFromDomainDTO;

/**
 * @author Ala Matos
 */
public class TransactionWithOwnerInfoFromDomainDTOAssembler {

    /**
     * Instantiates a new dto from Domain types TransactionWithOwnerInfoFromDomainDTO
     *
     * @param transactionFromDomain
     * @param ownerInfo
     * @return
     */
    public static TransactionWithOwnerInfoFromDomainDTO createTransaction(Transaction transactionFromDomain, String ownerInfo) {

        TransactionWithOwnerInfoFromDomainDTO transaction = new TransactionWithOwnerInfoFromDomainDTO();
        transaction.setOwnerInfo(ownerInfo);
        transaction.setCategory(transactionFromDomain.getCategoryID().getDenomination().getDenomination());
        transaction.setType(transactionFromDomain.getType().getType());
        transaction.setDescription(transactionFromDomain.getDescription().getDescription());
        transaction.setAmount(transactionFromDomain.getAmount().getAmount());
        transaction.setDate(transactionFromDomain.getDate().getDate().toString());
        transaction.setDebitAccount(transactionFromDomain.getDebitAccountID().getDenomination().getDenomination());
        transaction.setCreditAccount(transactionFromDomain.getCreditAccountID().getDenomination().getDenomination());

        return transaction;
    }
}

