# SWitCH Assessment DSOFT2 - 1191772 #

Name: José Miguel Pereira

ISEP number: 1191772

This is the repository used for the individual assessment for SWitCH, 2019-2020 Edition.

## What is this repository for? ##

It contains the end-to-end resolution for the following User Story (US) / functionality:

* US 10 (obrigatória) - "Como membro do grupo 1 e administrador do grupo 2, pretendo copiar 
(duplicar) as categorias do grupo 1 para o grupo 2, excluindo aquelas que não são aplicadas em 
movimentos no grupo 1."


## Resolution - Approach ##

To approach this US, I started by designing the Class Diagram (CD) and the Sequence Diagram (SD)
before implementing the solution.

First of all, here is the Logical View of our project architecture at three different levels of
granularity:

* 1 - Context;
* 2 - Containers;
* 3 - Components;

![SDP](diagrams/logical.jpeg)


#### System Sequence Diagram (SSD) ####

Below, I present my design of the SDD to approach the required US, showing the interaction between
the actor and the system.

![ssd](diagrams/ssd.png)


#### Class Diagram (CD) ####

Below, after analysing the requirements, I present my design of the CD to approach the required US from a static point of view.

![cd](diagrams/cd.png)


#### Sequence Diagram (SD) ####

Below, after analysing the requirements, I present my design of the CD to approach the required US from a dynamic point of view.

![sd](diagrams/sd.png)


#### Implementation ####

After designing the solutions, I've implemented the code accordingly in the backend and in the frontend. 
we can check the final result at our localhost:

![frontend](diagrams/copycategory.png)

The user "Paulo" is an Admin of the Group "Sunday Runners" and is making a copy of an existing category
to the group "Fontes Family", fullfiling the initial requirements.


## Final Notes ##

Every new class/method implemented throughout this assessment has a coverage of 100% (almost...).

For any questions, please contact me at 1191772@isep.ipp.pt or via Teams :)

Thank you for your time,

Miguel